# README #

Algorithm Engineering Course at Aarhus University

### What is this repository for? ###

* Project 1,2 and 3 of the above
Project 1: Binary search for predecessor queries
project 2: matrix multiplication
project 3: rank/select data structures

### How do I get set up? ###

clone the repository. Each project has its own folder and makefile, and its own readme. compile with gcc. 
Developed on Ubuntu and Fedora. No guarantee for gcc on windows. gcc version 4.8.2

### Contribution guidelines ###

