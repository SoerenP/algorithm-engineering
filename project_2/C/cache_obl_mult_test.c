#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include "matrix.h"

int main(int argc, char **argv){
  srand(time(NULL));
  struct Matrix* A = random_matrix(3,3,5);
  struct Matrix* B = random_matrix(3,2,5);
  struct Matrix* C = cache_obl_mult(A,B,row_major);
  int i,j;
  printf("\n");
  for (i = 0; i < A->rows; i++){
    for (j = 0; j < A->cols; j++){
      printf("%d ", get_value(A,i,j,row_major));
    }
    printf("\n");
  }
  printf("\n");
  for (i = 0; i < B->rows; i++){
    for (j = 0; j < B->cols; j++){
      printf("%d ", get_value(B,i,j,row_major));
    }
    printf("\n");
  }
  printf("\n");
  for (i = 0; i < C->rows; i++){
    for (j = 0; j < C->cols; j++){
      printf("%d ", get_value(C,i,j,row_major));
    }
    printf("\n");
  }
  printf("\n");
}
