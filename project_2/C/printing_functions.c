#include <stdio.h>
#include <time.h>
#include <assert.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include "printing_functions.c"

void print_ll_ToCSV(int *sizes, long long *data, const char *filename, int size)
{
	FILE *fp;
	int i;

	fp=fopen(filename, "w+");
	if(fp){
		for(i = 0; i < size; i++){
			fprintf(fp,"%d, %lld\n",sizes[i],data[i]);
		}
		fclose(fp);
	}
}

void print_f_ToCSV(int *sizes, double data[], const char *filename, int size)
{
	FILE *fp;
	int i;

	fp=fopen(filename, "w+");
	if(fp){
		for(i = 0; i < size; i++){
			fprintf(fp,"%d, %f\n",sizes[i],data[i]);
		}
		fclose(fp);
	}
}
