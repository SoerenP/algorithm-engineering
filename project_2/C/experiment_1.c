#include <stdio.h>
#include <time.h>
#include <assert.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <limits.h>
#include <math.h>
#include "matrix.h"
#include "papi_functions.h"
#include "papi.h"
#include "../../src/C/file_io.h"

#define BRANCH_RES_PATH "../../src_project_2/C/test_results/branch/"
#define L1_RES_PATH "../../src_project_2/C/test_results/l1_cache/"
#define L2_RES_PATH "../../src_project_2/C/test_results/l2_cache/"
#define TIME_RES_PATH "../../src_project_2/C/test_results/time/"
#define INSTR_RES_PATH "../../src_project_2/C/test_results/instr/"


/* Setup arrays of events to PAPI */
#define event_number 1

long long values[event_number]; //Array for results
int events[event_number] = {PAPI_L1_DCM};//, PAPI_L2_DCM, PAPI_TLB_TL, PAPI_BR_MSP};

double time_mat_mult(struct Matrix* A, struct Matrix* B, MEM_LAYOUT ml, mat_mult mult){

clock_t begin, end;
double time_spent;

  /* start counting */
  begin = clock();

  /* do work */
  struct Matrix* C = mult(A,B,ml);

  /*stop counting */
  end = clock();

  free_matrix(C);
  time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
  return time_spent;
}

long long L1_misses_mult(struct Matrix* A, struct Matrix* B, MEM_LAYOUT ml, mat_mult mult){
  long long values[2]; //Array for results
  int events[1] = {PAPI_L1_DCM};
  init_PAPI(events,1);
  struct Matrix* C = mult(A,B,ml);
  stop_PAPI(values,1);
  free_matrix(C);
  return values[0];
}

long long L2_misses_mult(struct Matrix* A, struct Matrix* B, MEM_LAYOUT ml, mat_mult mult){
  long long values[2]; //Array for results
  int events[1] = {PAPI_L2_DCM};
  init_PAPI(events,1);
  struct Matrix* C = mult(A,B,ml);
  stop_PAPI(values,1);
  free_matrix(C);
  return values[0];
}

long long TLB_misses_mult(struct Matrix* A, struct Matrix* B, MEM_LAYOUT ml, mat_mult mult){
  long long values[2]; //Array for results
  int events[1] = {PAPI_TLB_TL};
  init_PAPI(events,1);
  struct Matrix* C = mult(A,B,ml);
  stop_PAPI(values,1);
  free_matrix(C);
  return values[0];
}

long long branch_mispred_mult(struct Matrix* A, struct Matrix* B, MEM_LAYOUT ml, mat_mult mult){
long long values[2]; //Array for results
  int events[1] = {PAPI_BR_MSP};
  init_PAPI(events,1);
  struct Matrix* C = mult(A,B,ml);
  stop_PAPI(values,1);
  free_matrix(C);
  return values[0];
}

long long total_instructions_mult(struct Matrix* A, struct Matrix* B, MEM_LAYOUT ml, mat_mult mult){
  long long values[2]; //Array for results
  int events[1] = {PAPI_TOT_INS};
  init_PAPI(events,1);
  struct Matrix* C = mult(A,B,ml);
  stop_PAPI(values,1);
  free_matrix(C);
  return values[0];
}

//, PAPI_L2_DCM, PAPI_TLB_TL, PAPI_BR_MSP};


void run_test(mat_mult mult, MEM_LAYOUT ml, const char *filename)
{
  /* setup values for data gathering */
  int i,j,k;
  int const high = 46;
  int const low = 10;
  int const iter = 10;
  
  int sizes[high-low];
  double times[high-low][iter];
  long long L1[high-low][iter];
  long long L2[high-low][iter];
  //long long TLB[high-low];
  long long BR_MSP[high-low][iter];
  long long TOT_INS[high-low][iter];
	
  j = low;
  for(i=j*j; j < high; j++,i=j*j){
    sizes[j-low] = i;
    for (k = 0; k < iter; k++){

      struct Matrix* A = random_matrix(i,i,INT_MAX);
      struct Matrix* B = random_matrix(i,i,INT_MAX);
      times[j-low][k] = time_mat_mult(A, B, ml, mult);
      printf("Ran mult on two %d x %d matrices, and it took %f seconds\n", i,i,times[j-low][k]);
      L1[j-low][k] = L1_misses_mult(A, B, ml, mult);
      printf("L1 cache misses: %lld\n", L1[j-low][k]);
      L2[j-low][k] = L2_misses_mult(A, B, ml, mult);
      printf("L2 cache misses: %lld\n", L2[j-low][k]);
      //TLB[j] = TLB_misses_mult(A, B, ml, mult);
      //printf("TLB cache misses: %lld\n", TLB[j]);
      BR_MSP[j-low][k] = branch_mispred_mult(A, B, ml, mult);
      printf("Branch mispredictions: %lld\n", BR_MSP[j-low][k]);
      TOT_INS[j-low][k] = total_instructions_mult(A, B, ml, mult);
      printf("Total number of instructions: %lld\n", TOT_INS[j-low][k]);
      free_matrix(A);
      free_matrix(B);
    }
  }
  /* write results to csv files */
  char timefile[128];
  sprintf(timefile, "%s%s", TIME_RES_PATH, filename);
  print_f_matrix_ToCSV(sizes,high-low,iter,times,timefile);

  char L1file[128];
  sprintf(L1file, "%s%s", L1_RES_PATH, filename);
  print_ll_matrix_ToCSV(sizes,high-low,iter,L1,L1file);

  char L2file[128];
  sprintf(L2file, "%s%s", L2_RES_PATH, filename);
  print_ll_matrix_ToCSV(sizes,high-low,iter,L2,L2file);

  char branchfile[128];
  sprintf(branchfile, "%s%s", BRANCH_RES_PATH, filename);
  print_ll_matrix_ToCSV(sizes,high-low,iter,BR_MSP,branchfile);

  char instrfile[128];
  sprintf(instrfile, "%s%s", INSTR_RES_PATH, filename);
  print_ll_matrix_ToCSV(sizes,high-low,iter,TOT_INS,instrfile);

}

int main(int argc, char **argv){
  
  
  /* seed randomizer, ONLY ONCE! */
  srand(time(NULL));

  if (argc == 2){
    switch(argv[1][0]){
    case '1':
      printf("\n\n*******RUNNING TEST ON NAIVE MULT v2********\n\n");
      run_test(naive_mult_v2, row_major, "naive_mult_v2.csv");
      break;
    case '2':
      printf("\n\n*******RUNNING TEST ON MULT V3********\n\n");
      run_test(mult_v3, row_major, "mult_v3.csv");
      break;
    case '3':
      printf("\n\n*******RUNNING TEST ON MULT V4********\n\n");
      run_test(mult_v4, row_major, "mult_v4.csv");
      break;
    case '4':
      printf("\n\n*******RUNNING TEST ON MULT V2********\n\n");
      run_test(mult_v2, row_major, "mult_v2.csv");
      break;
    case '5':
      printf("\n\n*******RUNNING TEST ON NAIVE MULT v1********\n\n");
      run_test(naive_mult_v1, row_major, "naive_mult_v1.csv");
      break;
    case '6':
      printf("\n\n*******RUNNING TEST ON MULT V1********\n\n");
      run_test(mult_v1, row_major, "mult_v1.csv");
      break;
    }
  } else {
    printf("Call the experiment with one of the following numbers in order to gather data\n\n1: Gather data on naive mult v2\n2: Gather data on mult v3\n3: Gather data on mult v4\n4: Gather data on mult v2\n5: Gather data on naive mult v1\n6: Gather data on mult v1\n");
  }
  return -1;
}
