#ifndef _partition_stack_h
#define _partition_stack_h
#define STACK_MAX 512
#include "matrix.h"

struct PartitionStack{
  struct MatrixPartitionComp partitions[STACK_MAX];
  unsigned short size;
};

void init(struct PartitionStack* stack);

struct MatrixPartitionComp* peek(struct PartitionStack* stack);

void push(struct PartitionStack* stack, struct MatrixPartitionComp p);

struct MatrixPartitionComp* pop(struct PartitionStack* stack);

//void free_stack(struct PartitionStack* stack);

#endif
