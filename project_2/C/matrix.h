#ifndef _matrix_h
#define _matrix_h

struct Matrix
{
  int rows;
  int cols;
  int size;

  int* arr;
};

struct SplitMatrix
{
  struct Matrix* first;
  struct Matrix* second;
};

struct MatrixPartition
{
  int x_offset;
  int y_offset;
  int rows;
  int cols;
};

struct MatrixPartitionComp
{
  unsigned int A_offset;
  unsigned int B_offset;
  unsigned int C_offset;
  unsigned short A_cols;
  unsigned short A_rows;
  unsigned short B_cols;
  unsigned short B_rows;
  unsigned short C_cols;
  unsigned short C_rows;
};



typedef enum 
  { 
    row_major = 1, 
    col_major = 2, 
    tiled = 3, 
    bit_interleaved = 4 
  } MEM_LAYOUT; 

typedef struct Matrix *(*mat_mult)(struct Matrix* A, struct Matrix* B, MEM_LAYOUT ml);

int get_value(struct Matrix* A, int x, int y, MEM_LAYOUT ml);

int get_index(struct Matrix* A, int x, int y, MEM_LAYOUT ml);
 
struct Matrix* random_matrix(int rows, int cols, int max_num);

struct Matrix* naive_mult_v1(struct Matrix* A, struct Matrix* B, MEM_LAYOUT ml);

struct Matrix* naive_mult_v2(struct Matrix* A, struct Matrix* B, MEM_LAYOUT ml);

struct Matrix* mult_v1(struct Matrix* A, struct Matrix* B, MEM_LAYOUT ml);

void rec_mult_v1(struct Matrix* A, struct Matrix* B, struct Matrix** C, MEM_LAYOUT ml);

struct Matrix* mult_v2(struct Matrix* A, struct Matrix* B, MEM_LAYOUT ml);

void rec_mult_v2(struct Matrix* A, struct MatrixPartition A_part,
		 struct Matrix* B, struct MatrixPartition B_part,
		 struct Matrix* C, struct MatrixPartition C_part, MEM_LAYOUT ml);


struct Matrix* mult_v3(struct Matrix* A, struct Matrix* B, MEM_LAYOUT ml);

void rec_mult_v3(struct Matrix* A, struct MatrixPartition A_part,
		 struct Matrix* B, struct MatrixPartition B_part,
		 struct Matrix* C, struct MatrixPartition C_part, MEM_LAYOUT ml);

struct Matrix* mult_v4(struct Matrix* A, struct Matrix* B, MEM_LAYOUT ml);

struct Matrix* vert_concat(struct Matrix* A, struct Matrix* B, MEM_LAYOUT ml);

struct Matrix* horz_concat(struct Matrix* A, struct Matrix* B, MEM_LAYOUT ml);

struct SplitMatrix* vert_split(struct Matrix* A, MEM_LAYOUT ml);

struct SplitMatrix* horz_split(struct Matrix* A, MEM_LAYOUT ml);

void add(struct Matrix* A, struct Matrix* B);

void free_matrix(struct Matrix* A);

void free_split_matrix(struct SplitMatrix* A);

#endif
