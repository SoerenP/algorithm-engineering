#include <stdlib.h>
#include <stdio.h>
#include "partition_stack.h"


void init(struct PartitionStack* stack){
  stack->size = 0;
}

struct MatrixPartitionComp* peek(struct PartitionStack* stack){
  if (stack->size > 0){
    return &(stack->partitions[stack->size-1]);
  }
  fprintf(stderr,"Error! Stack is empty!\n"); 
  return malloc(sizeof(struct MatrixPartitionComp));
}

void push(struct PartitionStack* stack, struct MatrixPartitionComp p){
  if (stack->size < STACK_MAX){
    stack->partitions[stack->size++] = p;
  }
  else
    fprintf(stderr,"Error! Stack is full!\n");
}

struct MatrixPartitionComp* pop(struct PartitionStack* stack){

  if (stack->size > 0)
    return &(stack->partitions[--stack->size]);
  else{
    fprintf(stderr, "Error! Stack is empty!\n");
    return malloc(sizeof(struct MatrixPartitionComp));
  }

}

/* void free_stack(struct PartitionStack* stack){
   free(stack->partitions);
   free(stack);
 };
*/
