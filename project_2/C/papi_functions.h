#ifndef _papi_functions_h
#define _papi_functions_h

void stop_PAPI(long long *values, int eventsize);

void init_PAPI(int *events, int eventsize);

void test_fail(char *file, int line, char *call, int retval);

#endif
