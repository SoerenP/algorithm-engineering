#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include "matrix.h"

int main(int argc, char **argv){
  struct Matrix* A = random_matrix(8,4,8);
  int i,j;
  printf("\n");
  for (i = 0; i < A->rows; i++){
    for (j = 0; j < A->cols; j++){
      printf("%d ", get_value(A,i,j,row_major));
    }
    printf("\n");
  }
  printf("\n");
  

  struct SplitMatrix* s = vert_split(A,row_major);
  
  for (i = 0; i < s->first->rows; i++){
    for (j = 0; j < s->first->cols; j++){
      printf("%d ", get_value(s->first,i,j,row_major));
    }
    printf("\n");
  }
  printf("\n");
  for (i = 0; i < s->second->rows; i++){
    for (j = 0; j < s->second->cols; j++){
      printf("%d ", get_value(s->second,i,j,row_major));
    }
    printf("\n");
  }
  printf("\n");
}
