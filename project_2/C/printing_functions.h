#ifndef _printing_functions_h
#define _printing_functions_h

void print_ll_ToCSV(int *sizes, long long *data, const char *filename, int size);

void print_f_ToCSV(int *sizes, double data[], const char *filename, int size);

#endif
