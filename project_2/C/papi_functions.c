#include <stdio.h>
#include <time.h>
#include <assert.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <limits.h>
#include <math.h>
#include "papi.h"



void test_fail(char *file, int line, char *call, int retval)
{
	fprintf(stderr,"%s\tFAILED\nLine # %d\n", file, line);
	if(retval == PAPI_ESYS){
		char buf[128];
		memset(buf, '\0', sizeof(buf));
		sprintf(buf, "System error in %s:", call);
		perror(buf);
	}
	else if ( retval > 0) {
		fprintf(stderr,"Error calculating: %s\n", call);
	} 
	else {
		fprintf(stderr,"Error in %s: %s\n", call, PAPI_strerror(retval));
	}
	fprintf(stderr,"\n");
	exit(1);
}

void stop_PAPI(long long *values, int eventsize)
{
	int ret;
	if((ret = PAPI_stop_counters(values,eventsize)) != PAPI_OK) test_fail(__FILE__,__LINE__,"PAPI_stop_counters", ret);
}

void init_PAPI(int *events, int eventsize)
{
  if(PAPI_num_counters() < eventsize) {
    fprintf(stderr, "No hardware counters here, or PAPI not supportned\n");
    exit(1);
  }
	int ret;	
	if((ret = PAPI_start_counters(events,eventsize)) != PAPI_OK) test_fail(__FILE__,__LINE__,"PAPI_start_counters", ret);
}
