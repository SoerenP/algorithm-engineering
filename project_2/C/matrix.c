#include "matrix.h"
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include "string.h"
#include "partition_stack.h"

int get_value(struct Matrix* A, int x, int y, MEM_LAYOUT ml){

  switch (ml){
  case row_major:
    return A->arr[y+A->cols*x];
  case col_major:
    return A->arr[A->rows*y+x];
  case tiled:
    return -1; // TODO
  case bit_interleaved:
    return -1; // TODO
  default:
    return -1;
  }

}

int get_index(struct Matrix* A, int x, int y, MEM_LAYOUT ml){

  switch (ml){
  case row_major:
    return y+A->cols*x;
  case col_major:
    return A->rows*y+x;
  case tiled:
    return -1; // TODO
  case bit_interleaved:
    return -1; // TODO
  default:
    return -1;
  }

}

struct Matrix* random_matrix(int rows, int cols, int max_num){
  

  struct Matrix* C = malloc(sizeof(struct Matrix));
  C->size = rows*cols;
  C->arr = malloc(rows*cols*sizeof(int));
  C->rows = rows;
  C->cols = cols;

  int i;
  for (i = 0; i < C->size; i++){
    C->arr[i] = rand() % max_num;
  }

  return C;
}

struct Matrix* naive_mult_v1(struct Matrix* A, struct Matrix* B, MEM_LAYOUT ml){

  struct Matrix* C = malloc(sizeof(struct Matrix));

  if (A->cols != B->rows){
    printf("Dimension mismatch!\n");
    exit(1);
    return C;
  }
 
  C->rows = A->rows;
  C->cols = B->cols;
  C->size = A->rows*B->cols;
  C->arr = malloc(C->size*sizeof(int));
  int i,j,k;               
  for (i = 0; i < C->cols; i++){
    for (j = 0; j < C->rows; j++){
      int index = get_index(C,j,i,ml);
      for (k = 0; k < A->cols; k++){
	C->arr[index] += get_value(A,j,k,ml) * get_value(B,k,i,ml);
      }
    }
    
  }
  return C;
}

struct Matrix* naive_mult_v2(struct Matrix* A, struct Matrix* B, MEM_LAYOUT ml){

  struct Matrix* C = malloc(sizeof(struct Matrix));

  if (A->cols != B->rows){
    printf("Dimension mismatch!\n");
    exit(1);
    return C;
  }
 
  C->rows = A->rows;
  C->cols = B->cols;
  C->size = A->rows*B->cols;
  C->arr = malloc(C->size*sizeof(int));
  int i,j,k, a=0,b=0,c=0;               
  for (i = 0; i < B->cols; i++){
    c = i;
    a = 0;
    for (j = 0; j < A->rows; j++){
      b = i;
      for (k = 0; k < A->cols; k++){	
	C->arr[c] += A->arr[a] * B->arr[b];
	a++;
	b += B->cols; 
      }
      c += C->cols;
    } 
  }
  return C;
}


/*
  Mult v1 - first naive implementation of the cache oblivious algorithm from the paper
 */
struct Matrix* mult_v1(struct Matrix* A, struct Matrix* B, MEM_LAYOUT ml){
  
  struct Matrix* C = malloc(sizeof(struct Matrix));
  
  if (A->cols != B->rows){
    printf("Dimension mismatch!\n");
    exit(1);
    return C;
  }
 
  C->rows = A->rows;
  C->cols = B->cols;
  C->size = A->rows*B->cols;
  C->arr = malloc(C->size*sizeof(int));

  int i;
  for (i = 0; i < C->size; i++){
    C->arr[i] = 0;
  }

  rec_mult_v1(A,B,&C,ml);
  return C;
}

void rec_mult_v1(struct Matrix* A, struct Matrix* B, struct Matrix** C, MEM_LAYOUT ml){
  
  if (A->rows == 1 && B->rows == 1 && B->cols == 1){
    (*C)->arr[0] += A->arr[0]*B->arr[0];
  } else if (A->rows >= B->rows && A->rows >= B->cols){

    struct SplitMatrix* splitA = vert_split(A,ml);
    struct SplitMatrix* splitC = vert_split(*C,ml);

    rec_mult_v1(splitA->first, B, &splitC->first, ml); 
    rec_mult_v1(splitA->second, B,&splitC->second, ml);
    *C = vert_concat(splitC->first, splitC->second, ml);

    free_split_matrix(splitA);
    free_split_matrix(splitC);

  } else if (B->rows >= A->rows && B->rows >= B->cols){

    struct SplitMatrix* splitA = horz_split(A,ml);
    struct SplitMatrix* splitB = vert_split(B,ml);

    rec_mult_v1(splitA->first, splitB->first, C, ml);
    rec_mult_v1(splitA->second, splitB->second, C, ml);
    
    free_split_matrix(splitA);
    free_split_matrix(splitB);

  } else if (B->cols >= A->rows && B->cols >= B->rows){
    
    struct SplitMatrix* splitB = horz_split(B,ml);
    struct SplitMatrix* splitC = horz_split(*C,ml);
    
    rec_mult_v1(A,splitB->first,&splitC->first,ml);
    rec_mult_v1(A,splitB->second,&splitC->second,ml);
    
    *C = horz_concat(splitC->first,splitC->second, ml);
    
    free_split_matrix(splitB);
    free_split_matrix(splitC);
  }
}


/*
  Mult v2 
  featuC:
  - The multiplication is now done inplace
  - The condition for stopping the recursion is checked for last (0.5% improvement)
 */
struct Matrix* mult_v2(struct Matrix* A, struct Matrix* B, MEM_LAYOUT ml){

  struct Matrix* C = malloc(sizeof(struct Matrix));

  if (A->cols != B->rows){
    printf("Dimension mismatch!\n");
    exit(1);
    return C;
  }
 
  C->rows = A->rows;
  C->cols = B->cols;
  C->size = A->rows*B->cols;
  C->arr = malloc(C->size*sizeof(int));

  int i;
  for (i = 0; i < C->size; i++){
    C->arr[i] = 0;
  }
  struct MatrixPartition A_part,B_part,C_part;
  A_part.x_offset = 0;
  A_part.y_offset = 0;
  A_part.rows = A->rows;
  A_part.cols = A->cols;
  B_part.x_offset = 0;
  B_part.y_offset = 0;
  B_part.rows = B->rows;
  B_part.cols = B->cols;
  C_part.x_offset = 0;
  C_part.y_offset = 0;
  C_part.rows = C->rows;
  C_part.cols = C->cols;
  rec_mult_v2(A, A_part, B, B_part, C, C_part, ml);
  return C;
}

void rec_mult_v2(struct Matrix* A, struct MatrixPartition A_part,
		      struct Matrix* B, struct MatrixPartition B_part,
		      struct Matrix* C, struct MatrixPartition C_part, MEM_LAYOUT ml)
{
  int scalar = A_part.rows == 1 && B_part.rows == 1 && B_part.cols == 1;
  if (!scalar && A_part.rows >= B_part.rows && A_part.rows >= B_part.cols){

    struct MatrixPartition splitA_first, splitA_second, splitC_first, splitC_second;
    splitA_first.x_offset = A_part.x_offset;
    splitA_first.y_offset = A_part.y_offset;
    splitA_first.rows = A_part.rows/2;
    splitA_first.cols = A_part.cols;
    
    splitA_second.x_offset = A_part.x_offset+splitA_first.rows;
    splitA_second.y_offset = A_part.y_offset;
    splitA_second.rows = A_part.rows - splitA_first.rows;
    splitA_second.cols = A_part.cols;
    
    splitC_first.x_offset = C_part.x_offset;
    splitC_first.y_offset = C_part.y_offset;
    splitC_first.rows = C_part.rows/2;
    splitC_first.cols = C_part.cols;
    
    splitC_second.x_offset = C_part.x_offset+splitC_first.rows;
    splitC_second.y_offset = C_part.y_offset;
    splitC_second.rows = C_part.rows - splitC_first.rows;
    splitC_second.cols = C_part.cols;

    rec_mult_v2(A, splitA_first, B, B_part, C, splitC_first, ml);
    rec_mult_v2(A, splitA_second, B, B_part, C, splitC_second, ml);

  } else if (!scalar && B_part.rows >= A_part.rows && B_part.rows >= B_part.cols){

    struct MatrixPartition splitA_first, splitA_second, splitB_first, splitB_second;
    splitA_first.x_offset = A_part.x_offset;
    splitA_first.y_offset = A_part.y_offset;
    splitA_first.rows = A_part.rows;
    splitA_first.cols = A_part.cols/2;
    
    splitA_second.x_offset = A_part.x_offset;
    splitA_second.y_offset = A_part.y_offset+splitA_first.cols;
    splitA_second.rows = A_part.rows;
    splitA_second.cols = A_part.cols - splitA_first.cols;
    
    splitB_first.x_offset = B_part.x_offset;
    splitB_first.y_offset = B_part.y_offset;
    splitB_first.rows = B_part.rows/2;
    splitB_first.cols = B_part.cols;
    
    splitB_second.x_offset = B_part.x_offset + splitB_first.rows;
    splitB_second.y_offset = B_part.y_offset;
    splitB_second.rows = B_part.rows - splitB_first.rows;
    splitB_second.cols = B_part.cols;

    rec_mult_v2(A, splitA_first, B, splitB_first, C, C_part, ml);
    rec_mult_v2(A, splitA_second, B, splitB_second, C, C_part, ml);

  } else if (!scalar && B_part.cols >= A_part.rows && B_part.cols >= B_part.rows){
    
    struct MatrixPartition splitB_first, splitB_second, splitC_first, splitC_second;
    splitB_first.x_offset = B_part.x_offset;
    splitB_first.y_offset = B_part.y_offset;
    splitB_first.rows = B_part.rows;
    splitB_first.cols = B_part.cols/2;
    
    splitB_second.x_offset = B_part.x_offset;
    splitB_second.y_offset = B_part.y_offset+splitB_first.cols;
    splitB_second.rows = B_part.rows;
    splitB_second.cols = B_part.cols - splitB_first.cols;

    splitC_first.x_offset = C_part.x_offset;
    splitC_first.y_offset = C_part.y_offset;
    splitC_first.rows = C_part.rows;
    splitC_first.cols = C_part.cols/2;
    
    splitC_second.x_offset = C_part.x_offset;
    splitC_second.y_offset = C_part.y_offset+splitC_first.cols;
    splitC_second.rows = C_part.rows;
    splitC_second.cols = C_part.cols - splitC_first.cols;

    rec_mult_v2(A, A_part, B, splitB_first, C, splitC_first, ml);
    rec_mult_v2(A, A_part, B, splitB_second, C, splitC_second, ml);
  } else if (scalar){
    /*printf("Performed a scalar multiplication on coordinates (%d, %d) and (%d, %d) yielding %d * %d = %d\n", 
      A_part.x_offset, A_part.y_offset, B_part.x_offset, B_part.y_offset, 
      get_value(A, A_part.x_offset, A_part.y_offset, ml), 
      get_value(B, B_part.x_offset, B_part.y_offset, ml), 
      get_value(A, A_part.x_offset, A_part.y_offset, ml) *
      get_value(B, B_part.x_offset, B_part.y_offset, ml));*/
    C->arr[get_index(C, C_part.x_offset,C_part.y_offset, ml)] += 
      get_value(A, A_part.x_offset, A_part.y_offset, ml) *
      get_value(B, B_part.x_offset, B_part.y_offset, ml);
    /*C->arr[C_part.y_offset+C->cols*C_part.x_offset] +=
      A->arr[A_part.y_offset+A->cols*A_part.x_offset] *
      B->arr[B_part.y_offset+B->cols*B_part.x_offset];*/
    // C <- C + AB (scalar multiplication)
  } 
}

/*
  mult v3
  FeatuC:
  - Replaced i/2 with i>>1 so we get rid of a lot of multiplication.
  - Made a cutoff variable, where we simply do naive matrix multiplication instead of recursion in order to save the recursive calls for small values.
 */
struct Matrix* mult_v3(struct Matrix* A, struct Matrix* B, MEM_LAYOUT ml){

  struct Matrix* C = malloc(sizeof(struct Matrix));

  if (A->cols != B->rows){
    printf("Dimension mismatch!\n");
    exit(1);
    return C;
  }
 
  C->rows = A->rows;
  C->cols = B->cols;
  C->size = A->rows*B->cols;
  C->arr = malloc(C->size*sizeof(int));

  int i;
  for (i = 0; i < C->size; i++){
    C->arr[i] = 0;
  }
  struct MatrixPartition A_part,B_part,C_part;
  A_part.x_offset = 0;
  A_part.y_offset = 0;
  A_part.rows = A->rows;
  A_part.cols = A->cols;
  B_part.x_offset = 0;
  B_part.y_offset = 0;
  B_part.rows = B->rows;
  B_part.cols = B->cols;
  C_part.x_offset = 0;
  C_part.y_offset = 0;
  C_part.rows = C->rows;
  C_part.cols = C->cols;
  rec_mult_v3(A, A_part, B, B_part, C, C_part, ml);
  return C;
}

void rec_mult_v3(struct Matrix* A, struct MatrixPartition A_part,
		 struct Matrix* B, struct MatrixPartition B_part,
		 struct Matrix* C, struct MatrixPartition C_part, MEM_LAYOUT ml)
{
  int cutoff_val = 1280;
  int cutoff = A_part.rows < cutoff_val && B_part.rows < cutoff_val && B_part.cols < cutoff_val;
  if (!cutoff && A_part.rows >= B_part.rows && A_part.rows >= B_part.cols){
    
    //printf("Split A and C vertically\n");
    struct MatrixPartition splitA_first, splitA_second, splitC_first, splitC_second;
    splitA_first.x_offset = A_part.x_offset;
    splitA_first.y_offset = A_part.y_offset;
    splitA_first.rows = (A_part.rows+1)>>1;
    splitA_first.cols = A_part.cols;
    
    splitA_second.x_offset = A_part.x_offset+splitA_first.rows;
    splitA_second.y_offset = A_part.y_offset;
    splitA_second.rows = A_part.rows - splitA_first.rows;
    splitA_second.cols = A_part.cols;
    
    splitC_first.x_offset = C_part.x_offset;
    splitC_first.y_offset = C_part.y_offset;
    splitC_first.rows = (C_part.rows+1)>>1;
    splitC_first.cols = C_part.cols;
    
    splitC_second.x_offset = C_part.x_offset+splitC_first.rows;
    splitC_second.y_offset = C_part.y_offset;
    splitC_second.rows = C_part.rows - splitC_first.rows;
    splitC_second.cols = C_part.cols;

    rec_mult_v3(A, splitA_first, B, B_part, C, splitC_first, ml);
    rec_mult_v3(A, splitA_second, B, B_part, C, splitC_second, ml);

  } else if (!cutoff && B_part.rows >= A_part.rows && B_part.rows >= B_part.cols){
    
    //printf("Split A horizontally and B vertically\n");
    struct MatrixPartition splitA_first, splitA_second, splitB_first, splitB_second;
    splitA_first.x_offset = A_part.x_offset;
    splitA_first.y_offset = A_part.y_offset;
    splitA_first.rows = A_part.rows;
    splitA_first.cols = (A_part.cols+1)>>1;
    
    splitA_second.x_offset = A_part.x_offset;
    splitA_second.y_offset = A_part.y_offset+splitA_first.cols;
    splitA_second.rows = A_part.rows;
    splitA_second.cols = A_part.cols - splitA_first.cols;
    
    splitB_first.x_offset = B_part.x_offset;
    splitB_first.y_offset = B_part.y_offset;
    splitB_first.rows = (B_part.rows+1)>>1;
    splitB_first.cols = B_part.cols;
    
    splitB_second.x_offset = B_part.x_offset + splitB_first.rows;
    splitB_second.y_offset = B_part.y_offset;
    splitB_second.rows = B_part.rows - splitB_first.rows;
    splitB_second.cols = B_part.cols;

    rec_mult_v3(A, splitA_first, B, splitB_first, C, C_part, ml);
    rec_mult_v3(A, splitA_second, B, splitB_second, C, C_part, ml);

  } else if (!cutoff && B_part.cols >= A_part.rows && B_part.cols >= B_part.rows){
    
    //printf("Split B and C horizontally\n");
    struct MatrixPartition splitB_first, splitB_second, splitC_first, splitC_second;
    splitB_first.x_offset = B_part.x_offset;
    splitB_first.y_offset = B_part.y_offset;
    splitB_first.rows = B_part.rows;
    splitB_first.cols = (B_part.cols+1)>>1;
    
    splitB_second.x_offset = B_part.x_offset;
    splitB_second.y_offset = B_part.y_offset+splitB_first.cols;
    splitB_second.rows = B_part.rows;
    splitB_second.cols = B_part.cols - splitB_first.cols;

    splitC_first.x_offset = C_part.x_offset;
    splitC_first.y_offset = C_part.y_offset;
    splitC_first.rows = C_part.rows;
    splitC_first.cols = (C_part.cols+1)>>1;
    
    splitC_second.x_offset = C_part.x_offset;
    splitC_second.y_offset = C_part.y_offset+splitC_first.cols;
    splitC_second.rows = C_part.rows;
    splitC_second.cols = C_part.cols - splitC_first.cols;

    rec_mult_v3(A, A_part, B, splitB_first, C, splitC_first, ml);
    rec_mult_v3(A, A_part, B, splitB_second, C, splitC_second, ml);
  } else {
    //printf("Reached the cutoff\n");
    int i,j,k,a,b,c;
    int a_init = get_index(A, A_part.x_offset, A_part.y_offset, ml);
    int b_init = get_index(B, B_part.x_offset, B_part.y_offset, ml);
    int c_init = get_index(C, C_part.x_offset, C_part.y_offset, ml);
    //printf("Initial offsets are a: %d, b: %d and c: %d\n", a_init, b_init, c_init);
    //printf("Sizes are a_r: %d, a_c: %d, b_r: %d, b_c: %d, c_r: %d, c_c: %d\n", A_part.rows, A_part.cols, B_part.rows, B_part.cols, C_part.rows, C_part.cols);
    for (i = 0; i < B_part.cols; i++){
      c = c_init+i;
      a = a_init;
      for (j = 0; j < A_part.rows; j++){
	b = b_init + i;
	for (k = 0; k < A_part.cols; k++){
	  //printf("Reached index a: %d, b: %d and c: %d\n", a,b,c);
	  C->arr[c] += A->arr[a] * B->arr[b];
	  a++;
	  b += B->cols; 
	}
	a += A->cols - A_part.cols;
	c += C->cols;
      } 
    }
  } 
}

/*
  mult v4
  FeatuC:
  - Packed the matrix partitions into a single struct to save space
  - Replaced the recursive calls with a stack implementation
*/
struct Matrix* mult_v4(struct Matrix* A, struct Matrix* B, MEM_LAYOUT ml){

  struct Matrix* C = malloc(sizeof(struct Matrix));

  if (A->cols != B->rows){
    printf("Dimension mismatch!\n");
    exit(1);
    return C;
  }
 
  C->rows = A->rows;
  C->cols = B->cols;
  C->size = A->rows*B->cols;
  C->arr = malloc(C->size*sizeof(int));

  int i;
  for (i = 0; i < C->size; i++){
    C->arr[i] = 0;
  }
  struct MatrixPartitionComp p;
  p.A_offset = 0;
  p.B_offset = 0;
  p.C_offset = 0;
  p.A_rows = A->rows;
  p.A_cols = A->cols;
  p.B_rows = B->rows;
  p.B_cols = B->cols;
  p.C_rows = C->rows;
  p.C_cols = C->cols;

  //printf("Sizes are a_r: %d, a_c: %d, b_r: %d, b_c: %d, c_r: %d, c_c: %d\n", p.A_rows, p.A_cols, p.B_rows, p.B_cols, p.C_rows, p.C_cols);

  struct PartitionStack stack;
  init(&stack);
  int cutoff_val = 160;
  push(&stack,p);
  while (stack.size > 0)
    {
      //printf("Popped from stack\n");
      struct MatrixPartitionComp* part = pop(&stack);
      //printf("Sizes are a_r: %d, a_c: %d, b_r: %d, b_c: %d, c_r: %d, c_c: %d\n", part->A_rows, part->A_cols, part->B_rows, part->B_cols, part->C_rows, part->C_cols);

      int cutoff = part->A_rows < cutoff_val && part->B_rows < cutoff_val && part->B_cols < cutoff_val;
      if (!cutoff && part->A_rows >= part->B_rows && part->A_rows >= part->B_cols){
	//printf("Split A and C vertically\n");
	struct MatrixPartitionComp part1;
	struct MatrixPartitionComp part2;

	part1.A_offset = part->A_offset;
	part1.A_rows = (part->A_rows+1)>>1;
	part1.A_cols = part->A_cols;
    
	part2.A_offset = part->A_offset+part1.A_rows * A->cols;
	part2.A_rows = part->A_rows - part1.A_rows;
	part2.A_cols = part->A_cols;

	part1.B_offset = part->B_offset;
	part1.B_rows = part->B_rows;
	part1.B_cols = part->B_cols;

	part2.B_offset = part->B_offset;
	part2.B_rows = part->B_rows;
	part2.B_cols = part->B_cols;
    
	part1.C_offset = part->C_offset; 
	part1.C_rows = (part->C_rows+1)>>1;
	part1.C_cols = part->C_cols;

	part2.C_offset = part->C_offset + part1.C_rows * C->cols;
	part2.C_rows = part->C_rows - part1.C_rows;
	part2.C_cols = part->C_cols;

	push(&stack,part1);
	push(&stack,part2);
  
      } else if (!cutoff && part->B_rows >= part->A_rows && part->B_rows >= part->B_cols){
	//printf("Split A horizontally and B vertically\n");
	struct MatrixPartitionComp part1;
	struct MatrixPartitionComp part2;
      
	part1.A_offset = part->A_offset;
	part1.A_rows = part->A_rows;
	part1.A_cols = (part->A_cols+1)>>1;
    
	part2.A_offset = part->A_offset+part1.A_cols;
	part2.A_rows = part->A_rows;
	part2.A_cols = part->A_cols - part1.A_cols;

	part1.B_offset = part->B_offset;
	part1.B_rows = (part->B_rows+1)>>1;
	part1.B_cols = part->B_cols;

	part2.B_offset = part->B_offset + part1.B_rows * B->cols;
	part2.B_rows = part->B_rows - part1.B_rows;
	part2.B_cols = part->B_cols;
    
	part1.C_offset = part->C_offset; 
	part1.C_rows = part->C_rows;
	part1.C_cols = part->C_cols;

	part2.C_offset = part->C_offset;
	part2.C_rows = part->C_rows;
	part2.C_cols = part->C_cols;

	push(&stack,part1);
	push(&stack,part2);

      } else if (!cutoff && part->B_cols >= part->A_rows && part->B_cols >= part->B_rows){
	//printf("Split B and C horizontally\n");

	struct MatrixPartitionComp part1;
	struct MatrixPartitionComp part2;

	part1.A_offset = part->A_offset;
	part1.A_rows = part->A_rows;
	part1.A_cols = part->A_cols;
    
	part2.A_offset = part->A_offset;
	part2.A_rows = part->A_rows;
	part2.A_cols = part->A_cols;

	part1.B_offset = part->B_offset;
	part1.B_rows = part->B_rows;
	part1.B_cols = (part->B_cols+1)>>1;

	part2.B_offset = part->B_offset + part1.B_cols;
	part2.B_rows = part->B_rows;
	part2.B_cols = part->B_cols - part1.B_cols;
    
	part1.C_offset = part->C_offset; 
	part1.C_rows = part->C_rows;
	part1.C_cols = (part->C_cols+1)>>1;

	part2.C_offset = part->C_offset + part1.C_cols;
	part2.C_rows = part->C_rows;
	part2.C_cols = part->C_cols - part1.C_cols;
    
	push(&stack,part1);
	push(&stack,part2);

      } else {
	//printf("Reached the cutoff\n");
	int i,j,k,a,b,c;
	//printf("Initial offsets are a: %d, b: %d and c: %d\n", a_init, b_init, c_init);
	//printf("Sizes are a_r: %d, a_c: %d, b_r: %d, b_c: %d, c_r: %d, c_c: %d\n", part->A_rows, part->A_cols, part->B_rows, part->B_cols, part->C_rows, part->C_cols);
	for (i = 0; i < part->B_cols; i++){
	  c = part->C_offset+i;
	  a = part->A_offset;
	  for (j = 0; j < part->A_rows; j++){
	    b = part->B_offset + i;
	    for (k = 0; k < part->A_cols; k++){
	      //printf("Reached index a: %d, b: %d and c: %d\n", a,b,c);
	      C->arr[c] += A->arr[a] * B->arr[b];
	      a++;
	      b += B->cols; 
	    }
	    a += A->cols - part->A_cols;
	    c += C->cols;
	  } 
	}
      }
      
    }
  //free_stack(stack);
  return C;
}

struct Matrix* vert_concat(struct Matrix* A, struct Matrix* B, MEM_LAYOUT ml) {
  
  struct Matrix* C = malloc(sizeof(struct Matrix)); 
  if (A->cols != B->cols){
    printf("Dimension mismatch!\n");
    return C;
  }
  C->cols = A->cols;
  C->rows = A->rows + B->rows;
  C->size = A->size + B->size;
  C->arr = malloc(C->size*sizeof(int));
  switch (ml){ 
  case row_major:
    // In the case of a row major layout, just concatenate the arrays
    memcpy(C->arr, A->arr, A->size*sizeof(int));
    memcpy(C->arr+A->size, B->arr, B->size*sizeof(int));
    return C;
  case col_major:
    // In the case of a col major layout, make each new column equal the concatenation of the previous two.
    ;
    int i;
    for (i = 0; i < A->cols; i++){
      memcpy(C->arr+i*A->rows+i*B->rows, A->arr+i*A->rows, A->rows*sizeof(int));
      memcpy(C->arr+(i+1)*A->rows+i*B->rows, B->arr+i*B->rows, B->rows*sizeof(int));
    }
    return C;
  default:
    return C;
  }
  return C;
}

struct Matrix* horz_concat(struct Matrix* A, struct Matrix* B, MEM_LAYOUT ml) {
  struct Matrix* C = malloc(sizeof(struct Matrix)); 
  
  if (A->rows != B->rows){
    printf("Dimension mismatch!\n");
    exit(1);
    return C;
  }

  C->cols = A->cols + B->cols;
  C->rows = A->rows;
  C->size = A->size + B->size;
  C->arr = malloc(C->size*sizeof(int));
  switch (ml){ 
  case row_major:
    // In the case of a row major layout, make each new row equal the concatenation of the previous two.
    ;
    int i;
    for (i = 0; i < A->rows; i++){
      memcpy(C->arr+i*A->cols+i*B->cols, A->arr+i*A->cols, A->cols*sizeof(int));
      memcpy(C->arr+(i+1)*A->cols+i*B->cols, B->arr+i*B->cols, B->cols*sizeof(int));
    }
    return C;
  case col_major:
    // In the case of a col major layout, just concatenate the arrays
    memcpy(C->arr, A->arr, A->size*sizeof(int));
    memcpy(C->arr+A->size, B->arr, B->size*sizeof(int));
    return C;
  default:
    return C;
  }
  return C;
}
  

struct SplitMatrix* horz_split(struct Matrix* A, MEM_LAYOUT ml){

  struct SplitMatrix* C = malloc(sizeof(struct SplitMatrix));
  C->first = malloc(sizeof(struct Matrix));
  C->second = malloc(sizeof(struct Matrix));
  C->first->rows = A->rows;
  C->second->rows = A->rows;
  C->first->cols = (A->cols+1)/2;
  C->second->cols = A->cols/2;
  C->first->size = C->first->rows*C->first->cols;
  C->second->size = C->second->rows*C->second->cols;
  C->first->arr = malloc(C->first->size*sizeof(int));
  C->second->arr = malloc(C->second->size*sizeof(int));

  switch (ml){ 
  case row_major:
    // In the case of a row major layout, split each row
    ;
    int i;
    for (i = 0; i < A->rows; i++){
      memcpy(C->first->arr + i*C->first->cols, 
	     A->arr + i*A->cols, 
	     C->first->cols*sizeof(int));
      memcpy(C->second->arr + i*C->second->cols, 
	     A->arr +  i*A->cols + C->first->cols, 
	     C->second->cols*sizeof(int));
    }
    return C;
  case col_major:
    // In the case of a col major layout, just split the array
    memcpy(C->first->arr, A->arr, C->first->size*sizeof(int));
    memcpy(C->second->arr, A->arr+C->first->size, C->second->size*sizeof(int));
    return C;
  default:
    return C;
  }
  return C;
}

struct SplitMatrix* vert_split(struct Matrix* A, MEM_LAYOUT ml){
  
  struct SplitMatrix* C = malloc(sizeof(struct SplitMatrix));
  C->first = malloc(sizeof(struct Matrix));
  C->second = malloc(sizeof(struct Matrix));
  C->first->rows = (A->rows+1)/2;
  C->second->rows = A->rows/2;
  C->first->cols = A->cols;
  C->second->cols = A->cols;
  C->first->size = C->first->rows*C->first->cols;
  C->second->size = C->second->rows*C->second->cols;
  C->first->arr = malloc(C->first->size*sizeof(int));
  C->second->arr = malloc(C->second->size*sizeof(int));

  switch (ml){ 
  case row_major:
    // In the case of a col major layout, just split the array
    memcpy(C->first->arr, A->arr, C->first->size*sizeof(int));
    memcpy(C->second->arr, A->arr+C->first->size, C->second->size*sizeof(int));
    return C;
  case col_major:
    // In the case of a row major layout, split each column
    ;
    int i;
    for (i = 0; i < A->rows; i++){
      memcpy(C->first->arr + i*C->first->rows, 
	     A->arr + i*A->rows, 
	     C->first->rows*sizeof(int));
      memcpy(C->second->arr + i*C->second->rows, 
	     A->arr + i*A->rows + C->first->rows, 
	     C->second->rows*sizeof(int));
    }
    return C;
  default:
    return C;
  }
  return C;
}


void add(struct Matrix* A, struct Matrix* B){
  if (A->rows != B->rows || A->cols != B->cols){
    printf("Dimension mismatch!\n");
    return;
  }
  int i;
  for (i = 0; i < A->size; i++){
    A->arr[i] += B->arr[i];
  }
}

void free_matrix(struct Matrix* A){
  free(A->arr);
  free(A);
}

void free_split_matrix(struct SplitMatrix* A){
  free_matrix(A->first);
  free_matrix(A->second);
  free(A);
}

