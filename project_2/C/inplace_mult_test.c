#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include "matrix.h"

int main(int argc, char **argv){
  srand(time(NULL));

  if (argc == 5)
    {
      int m = atoi(argv[2]);
      int n = atoi(argv[3]);
      int p = atoi(argv[4]);
      
      struct Matrix* A = random_matrix(m,n,10000);
      struct Matrix* B = random_matrix(n,p,10000);
      struct Matrix* C;// = naive_mult_v1(A,B,row_major);
      //struct Matrix* C_v4 = mult_v4(A,B,row_major);
      struct Matrix* C_naive = naive_mult_v2(A,B,row_major);
      if (argv[1][0] == '1')
	{
	  C = mult_v1(A,B,row_major);
	}
      else if (argv[1][0] == '2')
	{
	  C = mult_v2(A,B,row_major);
	}
      else if (argv[1][0] == '3')
	{
	  C = mult_v3(A,B,row_major);
	}
      else if (argv[1][0] == '4')
	{
	  C = mult_v4(A,B,row_major);
	}
      else 
	{
	  printf("Invalid version!\n");
	  exit(1);
	}
      int i,j;
      for (i = 0; i < C->rows; i++){
	for (j = 0; j < C->cols; j++){
	  if (get_value(C,i,j,row_major) != get_value(C_naive,i,j,row_major)){
	    printf("Algorithm disagreed with naive on index %d, %d!!!\n", i,j);
	  }
	}
      }
      printf("All done! No prints means that there was no disagreements\n");

    }
  else
    {
      printf("Please use this test with the arguments v, m, n and p as in ./inplace_test v m n p, where v is the version of the algorithm you're trying to test and m, n and p are dimensions of the two matrices you want to multiply\n");
      exit(1);
    }
  /*printf("\n");
    for (i = 0; i < A->rows; i++){
    for (j = 0; j < A->cols; j++){
    printf("%d ", get_value(A,i,j,row_major));
    }
    printf("\n");
    }
    printf("\n");
    for (i = 0; i < B->rows; i++){
    for (j = 0; j < B->cols; j++){
    printf("%d ", get_value(B,i,j,row_major));
    }
    printf("\n");
    }
    printf("\n");
    for (i = 0; i < C->rows; i++){
    for (j = 0; j < C->cols; j++){
    printf("%d ", get_value(C,i,j,row_major));
    }
    printf("\n");
    }
    printf("\n");
    for (i = 0; i < C_v4->rows; i++){
    for (j = 0; j < C_v4->cols; j++){
    printf("%d ", get_value(C_v4,i,j,row_major));
    }
    printf("\n");
    }
    printf("\n");
    for (i = 0; i < C_naive->rows; i++){
    for (j = 0; j < C_naive->cols; j++){
    printf("%d ", get_value(C_naive,i,j,row_major));
    }
    printf("\n");
    }
    printf("\n");*/
}
