#ifndef _partition_stack_h
#define _partition_stack_h
#define STACK_MAX 64

struct PartitionStack{
  struct MatrixPartitionComp partitions[STACK_MAX];
  unsigned short size;
};

void init(struct PartitionStack* stack);

struct MatrixPartitionComp* peek(struct PartitionStack* stack);

void push(struct PartitionStack* stack, struct MatrixPartitionComp* partition);

struct MatrixPartitionComp* pop(struct PartitionStack* stack);

#endif
