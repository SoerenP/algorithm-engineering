#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include "matrix.h"

int main(int argc, char **argv){
  struct Matrix* A = random_matrix(3,3,8);
  struct Matrix* B = random_matrix(5,3,8);
  int i,j;
  printf("\n");
  for (i = 0; i < A->rows; i++){
    for (j = 0; j < A->cols; j++){
      printf("%d ", get_value(A,i,j,col_major));
    }
    printf("\n");
  }
  printf("\n");
  for (i = 0; i < B->rows; i++){
    for (j = 0; j < B->cols; j++){
      printf("%d ", get_value(B,i,j,col_major));
    }
    printf("\n");
  }

  struct Matrix* C = vert_concat(A,B,col_major);
  
  printf("\n");
  for (i = 0; i < C->rows; i++){
    for (j = 0; j < C->cols; j++){
      printf("%d ", get_value(C,i,j,col_major));
    }
    printf("\n");
  }
  printf("\n");
}
