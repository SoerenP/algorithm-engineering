set -e
#!/bin/bash
echo "BFS L1"
/home/soren/CompSci/AlgEng/src/C/exe/BFSL1 BFSL1_s.csv
echo "BFS l2"
/home/soren/CompSci/AlgEng/src/C/exe/BFSL2 BFSL2_s.csv
echo "BFS tlb"
/home/soren/CompSci/AlgEng/src/C/exe/BFStlb BFStlb_s.csv
echo "BFS branch"
/home/soren/CompSci/AlgEng/src/C/exe/BFSbranch BFSbranch_s.csv
echo "BFS instr"
/home/soren/CompSci/AlgEng/src/C/exe/BFSinstr BFSisntr_s.csv
echo "BFS time"
/home/soren/CompSci/AlgEng/src/C/exe/BFStime BFStime_s.csv


