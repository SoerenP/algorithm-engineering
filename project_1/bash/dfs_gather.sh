set -e
#!/bin/bash
echo "DFS L1"
/home/soren/CompSci/AlgEng/src/C/exe/DFSL1 DFSL1_s.csv
echo "DFS l2"
/home/soren/CompSci/AlgEng/src/C/exe/DFSL2 DFSL2_s.csv
echo "DFS tlb"
/home/soren/CompSci/AlgEng/src/C/exe/DFStlb DFStlb_s.csv
echo "DFS branch"
/home/soren/CompSci/AlgEng/src/C/exe/DFSbranch DFSbranch_s.csv
echo "DFS instr"
/home/soren/CompSci/AlgEng/src/C/exe/DFSinstr DFSinstr_s.csv
echo "DFS time"
/home/soren/CompSci/AlgEng/src/C/exe/DFStime DFStime_s.csv

