set -e
#!/bin/bash
echo "Naive L1"
/home/soren/CompSci/AlgEng/src/C/exe/NaiveL1 NaiveL1_s.csv
echo "Naive l2"
/home/soren/CompSci/AlgEng/src/C/exe/NaiveL2 NaiveL2_s.csv
echo "Naive tlb"
/home/soren/CompSci/AlgEng/src/C/exe/Naivetlb Naivetlb_s.csv
echo "Naive branch"
/home/soren/CompSci/AlgEng/src/C/exe/Naivebranch Naivebranch_s.csv
echo "Naive instr"
/home/soren/CompSci/AlgEng/src/C/exe/Naiveinstr Naiveinstr_s.csv
echo "Naive time"
/home/soren/CompSci/AlgEng/src/C/exe/Naivetime Naivetime_s.csv


