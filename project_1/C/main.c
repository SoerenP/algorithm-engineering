#include <stdio.h>
#include <time.h>
#include <assert.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <limits.h>
#include <math.h>
#include "dbg.h"
#include "naive_binary_search.h"
#include "balanced_binary_search.h"
#include "sorting.h"
#include "veb_implicit.h"
#include "project1.h"
#include "papi.h"
#include <time.h>
#include "file_io.h"
#include "papi_functions.h"

#define TIME 1 // if time is set to 1, we take time and write to double file, else we take events

#define SORT_ARRAY_PATH "/home/soren/CompSci/AlgEng/project_1/C/arrays/sorted"
#define BRANCH_RES_PATH "/home/soren/CompSci/AlgEng/project_1/C/test_results/branch/"
#define CACHE_RES_PATH "/home/soren/CompSci/AlgEng/project_1/C/test_results/cache/"
#define TIME_RES_PATH "/home/soren/CompSci/AlgEng/project_1/C/test_results/time/"
#define TLB_RES_PATH "/home/soren/CompSci/AlgEng/project_1/C/test_results/tlb/"
#define INSTR_RES_PATH "/home/soren/CompSci/AlgEng/project_1/C/test_results/instr/"

int M = 1000000; // number of queries

int main(int argc, char **argv){
	char *filename = argv[1];
	fprintf(stderr,"filename: %s\n",filename);

	/* Setup arrays of events to PAPI */
	long long values[2]; //Array for results
	int l1[1] = {PAPI_L1_DCM};
	int l2[1] = {PAPI_L2_DCM};
	int tlb[1] = {PAPI_TLB_TL};
	int branch[1] = {PAPI_BR_MSP};
	int instr[1] = {PAPI_TOT_INS};
	
	/* setup values for data gathering */
	int size, high, low, iter;
	low = 1;
	high = 25;
	iter = 10;
	long long int res[high-low][iter];
	double times[high-low][iter];
	double time_spent;
	int sizes[high-low];
	
	/* seed randomizer, ONLY ONCE! */
	srand(time(NULL));
	
	/* make stack-allocated array of queries as to not get cache faults on this part of the program */
	//int queries[M];
	int i,j,k;
	int *arr;
	int *queries;

	/* ******** do the following form low to high.. ********* */
	for(k = 0; k < iter; k++){
		j = 0;
		printf("Iter: %d\n",k);
		printf("Size: ");
		for(i=low; i <= high; i++,j++){
			size = (int)pow(2,i); 
			arr = getRandomArray(size);
			quick_sort(arr, size, sorted_order);
			queries = getRandomArray(M);		
			if(!arr || !queries) fprintf(stderr,"no array\n");
	
			printf(" %d ",i);

			if(!TIME){
				/* issue queries - measure events */
				//Pred_Naive_PAPI(queries, M, arr, size, values, instr, 1);
				Pred_RBT_PAPI(queries, M, arr, size, values, instr, 1);	
				//Pred_VEB_implicit_PAPI(queries, M, arr, size, values, l1, 1);
				//Pred_BFS_PAPI(queries, M, arr, size, values, instr, 1);			
				//Pred_DFS_PAPI(queries, M, arr, size, values, instr, 1);
				/* gather data - events */
				res[j][k] = values[0]; // if not gathering time but events, write to this	
			} else {
				/* issue queries - measure time */
				//time_spent = Pred_Naive_time(queries, M, arr, size);
				time_spent = Pred_RBT_time(queries, M, arr, size);
				//time_spent = Pred_BFS_time(queries, M, arr, size);
				//time_spent = Pred_DFS_time(queries, M, arr, size);
				/* gather data - time */
				times[j][k] = time_spent;
			}
			
			sizes[j] = i;
			if(arr) free(arr);
			if(queries) free(queries);
			}
		printf("\n");
		}

		/* write results to csv file */
		char resultfile[128];
		sprintf(resultfile, "%s%s", TIME_RES_PATH, filename);
		
		/* events - write long long */	
		if(!TIME) print_ll_matrix_ToCSV(sizes,high-low+1, iter, res, resultfile);
	
		/* time - write double */
		else print_f_matrix_ToCSV(sizes, high-low+1, iter, times, resultfile);
	

	return 0;
}
