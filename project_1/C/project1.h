#ifndef __project1_h
#define __project1_h


/* function pointers */
typedef int (*predec)(int *A, int count, int x);
typedef int *(*PredQuery)(int *targets, int *universe, int size);

/* prototyper */
void Agree_Pred1_vs_Pred2(int size, PredQuery pred1, PredQuery pred2,int flagWriteToFile);
void comparePredResFile(const char *f1, const char *f2);
void comparePredResArray(int *a1, int *a2, int size);

int *queuPredNaive(int *targets, int *universe, int size);
int *queuPredRBT(int *targets, int *universe, int size);

/* time */
double Pred_RBT_time(int targets[], int targetsize, int *universe, int unisize);
double Pred_BFS_time(int targets[], int targetsize, int *universe, int unisize);
double Pred_Naive_time(int targets[], int targetsize, int *universe, int unisize);
double Pred_DFS_time(int targets[], int targetsize, int *universe, int unisize);

/* counting wrappers for papi interaction */
void Pred_Naive_PAPI(int *targets, int targetsize, int *universe, int unisize, long long *values, int *events, int eventsize);
void Pred_RBT_PAPI(int targets[], int targetsize, int *universe, int unisize, long long *values, int *events, int eventsize);
void Pred_VEB_implicit_PAPI(int targets[], int targetsize, int *universe, int unisize, long long *values, int *events, int eventsize);
void Pred_BFS_PAPI(int targets[], int targetsize, int *universe, int unisize, long long *values, int *events, int eventsize);
void Pred_DFS_PAPI(int targets[], int targetsize, int *universe, int unisize, long long *values, int *events, int eventsize);

#endif
