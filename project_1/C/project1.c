#include <stdio.h>
#include <time.h>
#include <assert.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <limits.h>
#include <math.h>
#include "dbg.h"
#include "naive_binary_search.h"
#include "balanced_binary_search.h"
#include "sorting.h"
#include "veb_implicit.h"
#include "project1.h"
#include "veb_layout.h"
#include <time.h>
#include "file_io.h"
#include "bfs_layout.h"
#include "dfs_layout.h"
#include "papi_functions.h"

int *queuPredNaive(int *targets, int *universe, int size)
{
	int *temp = malloc(sizeof(int)*size);
	int i; 
	if(!temp){
		fprintf(stderr,"failed to alloc at qeuePred\n");
		return NULL;
	} else {
		for(i = 0; i < size; i++){
			temp[i] = naive_pred(universe,size,targets[i]);
		}
		return temp;
	}
}

int *queuPredRBT(int *targets, int *universe, int size)
{
	int i;
	struct RBTree *tree = createRBTree(universe[0]);
	int *preds = malloc(sizeof(int)*size);
	
	if((!tree) || (!preds)){
		fprintf(stderr,"Failed to make RBTree\n");
		return NULL;
	} else {
		for(i = 1; i < size; i++){
			RBT_insert(tree, createTreeNode(universe[i]));
		}

		for(i = 0; i < size; i++){
			preds[i] = RBT_pred(tree->root,targets[i],targets[i]);
		}

		RBT_free(tree);
		return preds;
	}
}

void Agree_Pred1_vs_Pred2(int size, PredQuery pred1, PredQuery pred2, int flagWriteToFile){
	int *test = getRandomArray(size);
	int *targets = getRandomArray(size);	
	assert((targets && test) != 0);
	int *sort = insertion_sort(test, size, sorted_order);


	int *res1, *res2;
	res1 = pred1(targets, sort, size);
	res2 = pred2(targets, sort, size);
	fprintf(stderr,"\n\n");

	if(flagWriteToFile != 0){
		printArrayToFile("temp_res1.txt", res1, size);
		printArrayToFile("temp_res2.txt", res2, size);

		comparePredResFile("temp_res1.txt", "temp_res2.txt");
	} else {
		comparePredResArray(res1, res2, size);
	}
	
	free(sort);
	free(test);
	free(targets);
	free(res1);
	free(res2);
}

void comparePredResArray(int *a1, int *a2, int size){
	int i,equal;
	equal = 1;
	for(i = 0; i < size; i++){
		if(a1[i] != a2[i]){
			fprintf(stderr,"comparePredRes: Disagree between %d and %d\n",a1[i],a2[i]);
			equal = 0;
		}
	}

	if(equal == 0) fprintf(stderr,"comparePredRes: Disagree\n");
	else fprintf(stderr,"comparePredRes: Agree\n");

}

void comparePredResFile(const char *f1, const char *f2){
	
	int size1,size2,i,equal; 
	FILE *fp1, *fp2;
	fp1=fopen(f1,"r+");
	fp2=fopen(f2,"r+");
	/* size of arrays are first number in file, check if equal */
	fscanf(fp1, "%d",&size1);
	fscanf(fp2, "%d",&size2);
	if(size1 != size2){
		fprintf(stderr,"Not same size, abort\n");
	}
	fclose(fp1);
	fclose(fp2);

	int *a1, *a2;
	a1 = getArrayFromFile(f1);
	a2 = getArrayFromFile(f2);
	assert((a1 && a2) != 0);
	
	equal = 1;
	for(i = 0; i < size1; i++){
		if(a1[i] != a2[i]){
			fprintf(stderr,"comparePredRes: Disagree between %d and %d\n",a1[i],a2[i]);
			equal = 0;
		}
	} 

	if(equal == 0) fprintf(stderr,"comparePredRes: Disagree\n");
	else fprintf(stderr,"comparePredRes: Agree\n");

	free(a1);
	free(a2);
}

double Pred_BFS_time(int targets[], int targetsize, int *universe, int unisize)
{
		/* setup what we dont want to count */
	clock_t begin, end;
	double time_spent;	
	int *bfs_array = malloc(unisize*sizeof(int));
	int i;

	if(!bfs_array){
		fprintf(stderr,"Could not malloac at Pred_BFS_PAPI\n");
		exit(1);		
	}
	layout_bfs(universe,bfs_array,1,0,unisize);

	begin = clock();
	for(i=0; i < targetsize; i++)
	{
		bfs_pred(targets[i],bfs_array, unisize, 0);
	}
	end = clock();
	time_spent = (double)(end - begin) / CLOCKS_PER_SEC;

	/* clean up */
	free(bfs_array);

	return time_spent;
}

double Pred_Naive_time(int targets[], int targetsize, int *universe, int unisize)
{
	clock_t begin, end;
	double time_spent;

	int i;
	begin = clock();
	for(i = 0; i < targetsize; i++){
		naive_pred(universe,unisize,targets[i]);
	}
	end = clock();
	time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
	return time_spent;

}

double Pred_DFS_time(int targets[], int targetsize, int *universe, int unisize)
{
	/* setup some ståff we dont wanna count */
	clock_t begin, end;
	double time_spent;
	int *dfs_array = malloc(unisize * sizeof(int));
	int *left_tree_sizes = malloc(unisize * sizeof(int));
	if(!(dfs_array) || !(left_tree_sizes))
	{
		fprintf(stderr,"Failed to malloc in Pred_DFS_PAPI\n");
		exit(1);
	}

	layout_dfs(universe, dfs_array, left_tree_sizes, 0, 0, unisize);
	int i;
	
	/* do work */
	begin=clock();
	for(i=0; i < targetsize; i++)
	{
		dfs_pred(targets[i], dfs_array, left_tree_sizes, unisize, 0);
	}
	end=clock();
	/* clean up */
	free(dfs_array);
	free(left_tree_sizes);

	time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
	return time_spent;

}

double Pred_RBT_time(int targets[], int targetsize, int *universe, int unisize)
{
	/* setup */
	clock_t begin, end;
	double time_spent;

	int i;
	struct RBTree *tree = createRBTree(universe[0]);
	if(!tree){
		fprintf(stderr,"Failed to create tree in Pred_RBT_PAPI\n");
		exit(1);
	}
	for(i = 1; i < unisize; i++){
		RBT_insert(tree, createTreeNode(universe[i]));
	}

	/* start counting */
	begin = clock();

	/* do work */
	for(i = 0; i < targetsize; i++){
		RBT_pred(tree->root,targets[i],targets[i]);
	}

	/*stop counting */
	end = clock();

	/* cleanup */
	RBT_free(tree);

	time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
	return time_spent;
}

void Pred_Naive_PAPI(int targets[], int targetsize, int *universe, int unisize, long long *values, int *events, int eventsize)
{
	/* setup we dont want to count */

	/* start counting */
	init_PAPI(events, eventsize);

	/* query! */
	int i;
	for(i = 0; i < targetsize; i++){
		naive_pred(universe,unisize,targets[i]);
	}

	/* stop counting */
	stop_PAPI(values, eventsize);

	/* clean up */
}

void Pred_RBT_PAPI(int targets[], int targetsize, int *universe, int unisize, long long *values, int *events, int eventsize)
{
	/* setup what we dont want to count - build the tree */
	struct RBTree *tree = createRBTree(universe[0]);
	if(!tree){
		fprintf(stderr,"Failed to create tree in Pred_RBT_PAPI\n");
		exit(1);
	}
	
	int i;
	for(i = 1; i < unisize; i++){
		RBT_insert(tree, createTreeNode(universe[i]));
	}

	/* start counting */
	init_PAPI(events, eventsize);
	

	/* query */
	for(i = 0; i < targetsize; i++){
		RBT_pred(tree->root,targets[i],targets[i]);
	}

	/* stop counting */
	stop_PAPI(values, eventsize);

	/* clean up */
	RBT_free(tree);
}

void Pred_BFS_PAPI(int targets[], int targetsize, int *universe, int unisize, long long *values, int *events, int eventsize)
{
	/* setup what we dont want to count */
	int *bfs_array = malloc(unisize*sizeof(int));
	if(!bfs_array){
		fprintf(stderr,"Could not malloac at Pred_BFS_PAPI\n");
		exit(1);		
	}
	int i;
	layout_bfs(universe,bfs_array,1,0,unisize);

	/* start counting */
	init_PAPI(events, eventsize);

	/* do work */
	for(i=0; i < targetsize; i++)
	{
		bfs_pred(targets[i],bfs_array, unisize, 0);
	}

	/* stop counting */
	stop_PAPI(values, eventsize);

	/* clean up */
	free(bfs_array);

}

void Pred_DFS_PAPI(int targets[], int targetsize, int *universe, int unisize, long long *values, int *events, int eventsize)
{
	/* setup some ståff we dont wanna count */
	int *dfs_array = malloc(unisize * sizeof(int));
	int *left_tree_sizes = malloc(unisize * sizeof(int));
	if(!(dfs_array) || !(left_tree_sizes))
	{
		fprintf(stderr,"Failed to malloc in Pred_DFS_PAPI\n");
		exit(1);
	}

	layout_dfs(universe, dfs_array, left_tree_sizes, 0, 0, unisize);
	int i;
	
	/* start papi */
	init_PAPI(events, eventsize);

	/* do work */
	for(i=0; i < targetsize; i++)
	{
		dfs_pred(targets[i], dfs_array, left_tree_sizes, unisize, 0);
	}

	/* stop papi */
	stop_PAPI(values, eventsize);

	/* clean up */
	free(dfs_array);
	free(left_tree_sizes);

}

void Pred_VEB_implicit_PAPI(int targets[], int targetsize, int *universe, int unisize, long long *values, int *events, int eventsize)
{
	/* setup */
	struct vEBImplicit *veb = build_veb(universe,unisize);
	if(!(veb) || !(universe)){
		fprintf(stderr,"Failed to create vEBImplicit\n");
		exit(1);		
	}
	int i;

	/* start counting */
	init_PAPI(events, eventsize);

	/* query */
	for(i=0; i < targetsize; i++){
		veb_layout_pred(targets[i], veb);
	}

	/* stop counting */
	stop_PAPI(values, eventsize);

	/* cleanup */
	//free_veb(veb);
	//free(veb->T);
  	//free(veb->B);
  	//free(veb->D);
}
