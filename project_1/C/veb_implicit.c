#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "veb_implicit.h"
#include "veb_layout.h"

struct vEBImplicit* build_veb(int *sorted_arr, int N)
{
  
  struct vEBImplicit* veb = malloc(sizeof(struct vEBImplicit));
  if (!check_complete_tree(N)){
    printf("Input was not a complete tree!\n");
    return veb;
  }
  veb->arr = malloc(N*sizeof(int));
  int i;
  for (i = 0; i < N; i++){
    veb->arr[i] = sorted_arr[i];
  }
  veb->size = N;

  // The height of the entire tree 
  veb->height = (int) ceil(log2((double) N));

  // Allocate space for the table
  veb->T = malloc(sizeof(int)*veb->height);
  veb->D = malloc(sizeof(int)*veb->height);
  veb->B = malloc(sizeof(int)*veb->height);    

  // Fill the table
  fill_table(veb,veb->height,0);

  // Set the values at depth 0 (initial depth)
  veb->B[0] = 0;
  veb->T[0] = 0;
  veb->D[0] = 0;

  layout_veb(veb);

  return veb;
    
}

// Fill the table consisting of T, D and B
void fill_table(struct vEBImplicit *veb, int rec_height, int rec_depth){
  // Check all the possible depths by moving through them in a binary search
  if (rec_height > 1){
    
    // Calculate the heights of the top tree and the bottom trees
    int top_height = (int)ceil((double)rec_height/2);
    int bottom_height = (int)floor((double)rec_height/2);

    // Get the index of the row we're trying to fill in the table
    int index = rec_depth + top_height;

    // Fill the table
    veb->B[index] = (int) pow(2.0,(double)bottom_height) - 1;
    veb->T[index] = (int) pow(2.0,(double)top_height) - 1;
    veb->D[index] = rec_depth;

    // Recurse through the top and bottom parts of the tree.
    fill_table(veb,top_height,rec_depth);
    fill_table(veb,bottom_height,index);
  } 
  
     
}

int check_complete_tree(int N){
  N = N+1;
  while (N > 2 && N%2 == 0){
    N = N/2;
  } 
  if (N == 2)
    return 1;
  else
    return 0;
}

void free_veb(struct vEBImplicit *veb){

  free(veb->T);
  free(veb->B);
  free(veb->D);
  free(veb->arr);
  free(veb);
}
