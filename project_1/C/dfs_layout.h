#ifndef _dfs_layout_h
#define _dfs_layout_h


int layout_dfs(int* input, int* dfs_array, int* left_tree_sizes, int dfs_index, int sorted_offset, int sorted_size);

int dfs_pred(int x, int* dfs_array, int* left_tree_sizes, int N, int debug);

#endif
