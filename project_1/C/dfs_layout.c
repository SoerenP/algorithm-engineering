#include "dfs_layout.h"
#include <math.h>
#include <stdlib.h>
#include <stdio.h>

int layout_dfs(int* input, int* res, int* left_tree_sizes, int dfs_index, int sorted_offset, int sorted_size){

  // This is the height up to where the tree can be filled completely
  int height = (int)floor(log2((double)(sorted_size+1)));

  // This is the minimum size of the two trees below this root
  int left_size = (int)pow(2.0,(double)(height-1))-1;
  int right_size = left_size;
  
  // This is the remaining number of nodes
  int rest = sorted_size-1-left_size-right_size;

  int next_left_layer_size = (int)pow(2.0, (double)(height-1)); 
  if (rest > next_left_layer_size){
    // If there are enough numbers left to fill a layer in the left tree, do so and add the rest to the right tree
    rest = rest - next_left_layer_size;
    left_size = left_size + next_left_layer_size;
    right_size = right_size + rest;
  } else if (rest <= next_left_layer_size){
    // If there aren't enough numbers left to fill an entire layer, then just add them to the left tree
    left_size = left_size + rest;
  }

  // Set the root node in this tree
  //printf("Accessed the %d'th index of the input\n", sorted_offset+left_size);
  res[dfs_index] = input[sorted_offset+left_size];
  //printf("DFS index was %d\n", dfs_index);
  left_tree_sizes[dfs_index] = left_size;

  dfs_index++;
  // If there are nodes in the left or right trees, recursively layout them
  if (left_size > 0){
    dfs_index = layout_dfs(input,res, left_tree_sizes, dfs_index,sorted_offset,left_size);
  }
  if(right_size > 0){
    dfs_index = layout_dfs(input,res, left_tree_sizes, dfs_index, sorted_offset+left_size+1, right_size);
  }
  return dfs_index;
}

int dfs_pred(int x, int* dfs_array, int* left_tree_sizes, int N, int debug){

  int dfs_index = 0;
  int current_best = -1;
  int height = (int)ceil(log2((double)N));
  
  int i;
  for (i = 0; i < height && dfs_index < N; i++){
    if (debug) printf("dfs index is %d\n", dfs_index);
    if (debug) printf("Compared %d to target: %d\n", dfs_array[dfs_index], x);
    if (dfs_array[dfs_index] > x){
      if(debug) printf("Went left\n");
      dfs_index = dfs_index+1;
    } else if (dfs_array[dfs_index] < x){
      if (debug) printf("Went right\n");
      if (current_best < dfs_array[dfs_index]){
	current_best = dfs_array[dfs_index];
      }
      if (debug) printf("Added %d to the dfs_index\n", left_tree_sizes[dfs_index]);
      dfs_index = dfs_index + left_tree_sizes[dfs_index]+1;
    } else if (dfs_array[dfs_index] == x){
      return x;
    }
  }

  return current_best;

}
