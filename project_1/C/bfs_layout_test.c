#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include "dbg.h"
#include "naive_binary_search.h"
#include "sorting.h"
#include "bfs_layout.h"

int N = 10043;
int M = 100000;
int runs = 100000;

int main(int argc, char **argv){

  srand(time(NULL));

  int *array = malloc(N*sizeof(int));
  int i;
  for (i = 0; i < N; i++){
    array[i] = rand() % M;
  }

  int* sorted_array = insertion_sort(array,N,sorted_order);

  int* bfs_array = malloc(N*sizeof(int));
  layout_bfs(sorted_array, bfs_array, 1, 0, N);

  int disagreements = 0;

  for (i = 0; i < runs; i++){
    int target = rand() %(M);
    //printf("Target is %d\n", target);
    int naive = naive_pred(sorted_array,N,target);
    
    int bfs_res = bfs_pred(target, bfs_array, N, 0);
    //int bfs_res = 0;
    if (naive == bfs_res){
      //printf("naive and veb agreed on %d, which they both got to %d\n", target, naive);
    } else {
      //bfs_pred(target,bfs_array,N,1);
      printf("naive and bfs DISAGREED on %d, where naive got %d and bfs got %d\n", target,naive,bfs_res);
      disagreements++;
    }    
  }
  printf("The number of disagreements is %d\n\n", disagreements);
  
  /*printf("Sorted order:\n");
  for (i = 0; i < N; i++){
    printf("%d, ", sorted_array[i]);
  }
  printf("\n\n");

  printf("Bfs order:\n");
  for (i = 0; i < N; i++){
    printf("%d, ", bfs_array[i]);
  }
  printf("\n");*/
  
  return 1;
}
