#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include "veb_implicit.h"
#include "veb_layout.h"
#include "dbg.h"
#include "naive_binary_search.h"
#include "sorting.h"

int N = (int)pow(2.0,5.0)-1;
int M = 1000;
int runs = 1000;

int main(int argc, char **argv){

  srand(time(NULL));

  int *array = malloc(N*sizeof(int));
  int i;
  for (i = 0; i < N; i++){
    array[i] = rand() % M;
  }

  int* sorted_array = quick_sort(array,N,sorted_order);
  
  struct vEBImplicit* veb = build_veb(sorted_array,N);

  int disagreements = 0;


  for (i = 0; i < runs; i++){
    int target = rand() %(M);
    //printf("Target is %d\n", target);
    int naive = naive_pred(sorted_array,N,target);
    int veb_res = veb_layout_pred(target,veb);
    
    if (naive == veb_res){
      //printf("naive and veb agreed on %d, which they both got to %d\n", target, naive);
    } else {
      printf("naive and veb DISAGREED on %d, where naive got %d and veb got %d\n", target,naive,veb_res);
      disagreements++;
    }

    
  }
  printf("The number of disagreements is %d\n\n", disagreements);
  /*for (i = 0; i < N; i++){
    printf("%d ",sorted_array[i]);
    }
    printf("\n\n");*/
  //free(array);
  free(sorted_array);
  free(veb->T);
  free(veb->B);
  free(veb->D);

  /*int *bfs = malloc(N*sizeof(int));
  layout_bfs(array, bfs, 1, 0, N);

  int *veb = malloc(N*sizeof(int));
  layout_veb_rec(bfs, veb, 0, 1, N);

  printf("Sorted order:\n");
  for (i = 0; i < N; i++){
    printf("%d, ", array[i]);
  }
  printf("\n\n");

  printf("Bfs order:\n");
  for (i = 0; i < N; i++){
    printf("%d, ", bfs[i]);
  }
  printf("\n");

  printf("veb order:\n");
  for (i = 0; i < N; i++){
    printf("%d, ", veb[i]);
  }
  printf("\n");*/
}
