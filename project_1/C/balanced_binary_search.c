#include <stdlib.h>
#include <stdio.h>
#include "balanced_binary_search.h"
#include <time.h>

#define TEST 1
struct RBTree *global_tree;

struct RBTree *createRBTree(int val)
{
	struct RBTree *new = malloc(sizeof(struct RBTree));
	if(new == NULL) return NULL;	
	struct TreeNode *root = createTreeNode(val);
	if(root == NULL) return NULL;	
	root->color = black;
	root->left = NULL;
	root->right = NULL;
	root->parent = NULL;
	
	// skal ikke være nul, skal bare være et blad der er sort?
	new->nill = NULL;
	new->root = root;
	return new;
}

void RBT_free_subtree(struct TreeNode *node)
{
	if(node != NULL){	
		if(node->left != NULL)
			RBT_free_subtree(node->left);
		if(node->right != NULL)
			RBT_free_subtree(node->right);
		free(node);
	}
}

void RBT_free(struct RBTree *tree)
{
	if(tree != NULL){
		struct TreeNode *root = tree->root;
		if(root->left != NULL)
			RBT_free_subtree(root->left);
		if(root->right != NULL)
			RBT_free_subtree(root->right);
		if(root != NULL)
			free(root);		
		free(tree);	
	}
}

struct TreeNode *createTreeNode(int val){
	struct TreeNode *new = malloc(sizeof(struct TreeNode));
	if(new == NULL) return NULL;	
	new->color = red;
	new->parent = NULL;
	new->left = NULL;
	new->right = NULL;
	new->val = val;
	return new;
}

void RBT_insert(struct RBTree *tree, struct TreeNode *z)
{
	struct TreeNode *y,*x;

	y = tree->nill;
	x = tree->root;
	while(x != tree->nill)
	{
		y = x;
		if(z->val < x->val) x = x->left;
		else x = x->right;	
	}
	z->parent = y;
	if(y == tree->nill) tree->root = z;
	else if(z->val < y->val) y->left = z;
	else y->right = z;

	z->left = tree->nill;
	z->right = tree->nill;
	z->color = red;
	RBT_insert_fixup(tree,z);
}

void RBT_insert_fixup(struct RBTree *tree, struct TreeNode *z)
{
	struct TreeNode *y;

	while((z->parent != NULL) && z->parent->color == red)
	{
		if(z->parent == z->parent->parent->left)
		{
			y = z->parent->parent->right;			
			if((y != NULL) && y->color == red) //Ups
			{
				z->parent->color = black;
				z->color = black;
				z->parent->parent->color = red;
				z = z->parent->parent;
			} else {
				if(z == z->parent->right){
					z = z->parent;			
					RBT_rotate_left(tree, z);
				}
				z->parent->color = black;
				z->parent->parent->color = red;
				RBT_rotate_right(tree,z->parent->parent);			
			} 
		} else {
			y = z->parent->parent->left;
			if((y != NULL) && y->color == red)
			{
				z->parent->color = black;
				y->color = black;
				z->parent->parent->color = red;
				z = z->parent->parent;
			} else {
				if(z == z->parent->left){
					z = z->parent;
					//RBT_rotate_left(tree, z);
					RBT_rotate_right(tree,z);
				}
				z->parent->color = black;
				z->parent->parent->color = red;
				//RBT_rotate_right(tree,z->parent->parent);
				RBT_rotate_left(tree,z->parent->parent);
			}
		}
	}
	tree->root->color = black;
}

void RBT_rotate_left(struct RBTree *tree, struct TreeNode *x)
{
	if(x->right == NULL) printf("ups");
	struct TreeNode *y;
	y = x->right;
	x->right = y->left;
	if(y->left != tree->nill) y->left->parent = x;
	y->parent = x->parent;
	if(x->parent == tree->nill) tree->root = y;
	else if(x == x->parent->left) x->parent->left = y;
	else x->parent->right = y;
	y->left = x;
	x->parent = y;
}
void RBT_rotate_right(struct RBTree *tree, struct TreeNode *x)
{
	if(x->left == NULL) printf("ups");
	struct TreeNode *y;
	y = x->left;
	x->left = y->right;
	if(y->right != tree->nill){y->right->parent = x;}
	y->parent = x->parent;
	if(x->parent == tree->nill) tree->root = y;
	else if(x == x->parent->right) x->parent->right = y;
	else x->parent->left = y;
	y->right = x;
	x->parent = y;
}

void RBT_transplant(struct RBTree *tree, struct TreeNode *u, struct TreeNode *v)
{
	if(u->parent == tree->nill) tree->root = v;
	else if(u == u->parent->left) u->parent->left = v;
	else u->parent->right = v;
	v->parent = u->parent;
}

void RBT_delete(struct RBTree *tree, struct TreeNode *z)
{
	struct TreeNode *y,*x;
	y = z;
	
	int y_orig_color = (int)y->color; //dirtyhack

	if(z->left == tree->nill){
		x = z->right;
		RBT_transplant(tree,z,z->right);
	} else if(z->right == tree->nill) {
		x = z->left;
		RBT_transplant(tree,z,z->left);
	} else {
		y = RBT_min(z->right);
		y_orig_color = y->color;
		x = y->right;
		if(y->parent == z) x->parent = y;
		else {
			RBT_transplant(tree,y,y->right);
			y->right = z->right;
			y->right->parent = y;
		}
		RBT_transplant(tree,z,y);
		y->left = z->left;
		y->left->parent = y;
		y->color = z->color;
	}
	if(y_orig_color == black) RBT_delete_fixup(tree,x);
}

void RBT_delete_fixup(struct RBTree *tree, struct TreeNode *x)
{
	struct TreeNode *w;
	while(x != tree->root && x->color == black)
	{
		if(x == x->parent->left){
			w = x->parent->right;
			if(w->color == red){
				w->color = black;
				x->parent->color = red;
				RBT_rotate_left(tree,x->parent);
				w = x->parent->right;
			}
			if(w->left->color == black && w->right->color == black){
				w->color = red;
				x = x->parent;
			} else {
				if(w->right->color == black){
					w->left->color = black;
					w->color = red;
					RBT_rotate_right(tree,w);
					w = x->parent->right;
				}
				w->color = x->parent->color;
				x->parent->color = black;
				w->right->color = black;
				RBT_rotate_left(tree,x->parent);
				x = tree->root;
			}
		} else {
			w = x->parent->left;
			if(w->color == red){
				w->color = black;
				x->parent->color = red;
				RBT_rotate_left(tree,x->parent);
				w = x->parent->left;
			}
			if(w->right->color == black && w->left->color == black)
			{
				w->color = red;
				x = x->parent;
			} else {
				if(w->left->color == black){
					w->right->color = black;
					w->color = red;
					RBT_rotate_left(tree,w);
					w = x->parent->left;
				}
				w->color = x->parent->color;
				x->parent->color = black;
				w->left->color = black;
				RBT_rotate_right(tree,x->parent);
				x = tree->root;
			}
		}
	x->color = black;
	}
}

struct TreeNode *RBT_iter_search(struct TreeNode *x, int k)
{
	while(x != NULL && k != x->val)
	{
		if(k < x->val) x = x->left;
		else x = x->right;
	}
	return x;
}

int RBT_pred(struct TreeNode *root, int val, int sofar)
{
	// vi fandt target, stop
	if(root->val == val) return root->val;

	//Hvis noden vi er på er mindre, skal vi til højre
	//hvis vi ikke kan komme til højre, er vi ved pred
	if(root->val < val && (root->right != NULL))
		return RBT_pred(root->right,val,root->val);
	else if(root->val < val && (root->right == NULL))
		return root->val;

	//Hvis noden vi er på er større, skal vi til venstre
	//Hvis vi ikke kan komme til venstre, er der intet element mindre, men elementet selv er der heller ikke
	if(root->val > val && (root->left != NULL))
		return RBT_pred(root->left,val,sofar);
	else if(root->val > val && (root->left == NULL))
		return sofar;
	return 0;
}

struct TreeNode *RBT_successor(struct TreeNode *x)
{
	struct TreeNode *y;
	y = RBT_iter_search(x, x->val);
	if(y != NULL)
			return y;
		else {

		if(x->right != NULL){
			return RBT_min(x->right);
		}
		//struct TreeNode *y;
		y = x->parent;
		while(y != NULL && x == y->right)
		{
			x = y;
			y = y->parent;
		}
		return y;
	}
}

struct TreeNode *RBT_predecessor(struct TreeNode *x)
{
	if(x->left != NULL)
	{
		printf("made it here");
		return RBT_max(x->left);
	}
	struct TreeNode *y;
	y = x->parent;
	while(y != NULL && x == y->left)
	{
		printf("made it the other place");
		x = y;
		y = y->parent;
	}
	return y;
}

struct TreeNode *RBT_max(struct TreeNode *root)
{
	while (root->right != NULL)
	{
		root = root->right;
	}
	return root;
}

struct TreeNode *RBT_min(struct TreeNode *root)
{
	while (root->left != NULL)
	{
		root = root->left;
	}
	return root;
}

void RBT_inorder_walk(struct TreeNode *root)
{
	if(root != NULL){
		RBT_inorder_walk(root->left);
		fprintf(stderr,"%d ",root->val);
		RBT_inorder_walk(root->right);	
	}
}

//Wrapper til pred

int pred(int x){
	fprintf(stderr,"Called pred?\n");
	return RBT_pred(global_tree->root,x,x);
}

void RBT_test_pred(){
	
	global_tree = createRBTree(5);
	if(global_tree && TEST){

		srand(time(NULL));
		int N = 100;
		int *array = malloc(sizeof(int)*N);
		if(array){
			int i,tmp;
			
			for(i = 0; i < N; i++){
				array[i] = rand() % 1000;
			}
			for(i = 0; i < N; i++){
				RBT_insert(global_tree, createTreeNode(array[i]));
			}
			printf("Inorder walk\n");
			RBT_inorder_walk(global_tree->root);
			printf("\n");
		
			printf("pred on entire set:\n");	
			for(i = 0; i < N; i++){
				printf("pred of %d is %d\n",array[i],pred(array[i]));
			}
			printf("\n");
			printf("Inorder walk\n");
			RBT_inorder_walk(global_tree->root);
			printf("\n");
			printf("pred on random values:\n");

			for(i = 0; i < N; i++){
				tmp = rand() % 1000;
				printf("pred of %d is %d\n",tmp,pred(tmp));
		}
		free(array);
	}	
	RBT_free(global_tree);			
	} else {
		//do nothing, malloc failed
	}
}
