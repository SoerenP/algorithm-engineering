#ifndef _papi_functions_h
#define _papi_functions_h

void test_fail(char *file, int line, char *call, int retval); //
void testPapiNaive(int *targets, int *universe, int size); //
void stop_PAPI(long long *values, int eventsize); //
void init_PAPI(int *events, int eventsize); //
void pred_PAPI(int *targets, int *universe, int testsize, PredQuery prec, long long *values, int *events, int eventsize);
void init_Time_PAPI(float *real_time, float *proc_time, float *mflops, long long *flpins);
float time_pred(int *targets, int *universe, int testsize, PredQuery pred);
double Pred_Naive_time(int targets[], int targetsize, int *universe, int unisize);


#endif
