function [] = compareResult(a,b,c,d)
A = csvread(a);
B = csvread(b);
C = csvread(c);
D = csvread(d);

A = normalizeData(A);
B = normalizeData(B);
C = normalizeData(C);
D = normalizeData(D);


semilogy(A(:,1),A(:,2),'-r');
hold on
semilogy(B(:,1),B(:,2),'--g');
hold on
semilogy(C(:,1),C(:,2),':b');
hold on
semilogy(D(:,1),D(:,2),'-.m');
display(a)
display('is red\n')
display(b)
display('is green\n')
display(c)
display('is blue\n')
display(d)
display('is magenta\n')
end

function [X_NORM] = normalizeData(X)
R = size(X(:,1));
R = R(1,1);
C = size(X(1,:));
C = C(1,2);
X_NORM = zeros(R(1),2);
X_NORM(:,1) = X(:,1);

for i=1:1:R
    X_NORM(i,2) = sum(X(i,:))/C; %tag gennemsnit
end
end