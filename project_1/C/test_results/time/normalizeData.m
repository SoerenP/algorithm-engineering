BFSTime = csvread('BFSTime.csv');

R = size(BFSTime(:,1));
R = R(1,1);
C = size(BFSTime(1,:));
C = C(1,2);
BFSTimeNorm = zeros(R(1),2);
BFSTimeNorm(:,1) = BFSTime(:,1);

for i=1:1:R
    BFSTimeNorm(i,2) = sum(BFSTime(i,:))/C; %tag gennemsnit
    BFSTimeNorm(i,2)=BFSTimeNorm(i,2)/log2(2^BFSTimeNorm(i,1)); %del med logaritmen
end

plot(BFSTimeNorm(:,1),BFSTimeNorm(:,2))

