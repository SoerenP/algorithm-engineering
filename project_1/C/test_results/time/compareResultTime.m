function [] = compareResultTime(a,b,c,d)
A = csvread(a);
B = csvread(b);
C = csvread(c);
D = csvread(d);

A = normalizeDataTime(A);
B = normalizeDataTime(B);
C = normalizeDataTime(C);
D = normalizeDataTime(D);


plot(A(:,1),A(:,2),'-r');
hold on
plot(B(:,1),B(:,2),'--g');
hold on
plot(C(:,1),C(:,2),':b');
hold on
plot(D(:,1),D(:,2),'-.m');
display(a)
display('is red\n')
display(b)
display('is green\n')
display(c)
display('is blue\n')
display(d)
display('is magenta\n')
end

function [X_NORM] = normalizeDataTime(X)
R = size(X(:,1));
R = R(1,1);
C = size(X(1,:));
C = C(1,2);
X_NORM = zeros(R(1),2);
X_NORM(:,1) = X(:,1);

for i=1:1:R
    X_NORM(i,2) = sum(X(i,:))/C; %tag gennemsnit
    X_NORM(i,2)=X_NORM(i,2)/log2(2^X_NORM(i,1)); %del med logaritmen
end
end