#ifndef __veb_h
#define __veb_h

#define universe 10000
struct vEB {
	int u;
	int min;
	int max;
	struct vEB *summary;
	struct vEB *cluster[universe];
};

//struct vEB cluster[universe]; //size is u_square of u

/* prototyper */

/* helpers to index into summary and clusters */
/* an element is described by [ high | low ] index */
/* high is the summary, low is the cluster */
int high(int x, struct vEB *tree);
int low(int x, struct vEB *tree);
int vEB_index(int x, int y, struct vEB *tree);
int l_square(int x);
int u_square(int x);

/* methods on the vEB structure */
int vEB_min(struct vEB *tree);
int vEB_max(struct vEB *tree);
int vEB_member(struct vEB *tree, int x);
void vEB_empty_insert(struct vEB *tree, int x);
void vEB_insert(struct vEB *tree, int x);
int vEB_pred(struct vEB *tree, int x);

#endif
