#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

int naive_pred(int *numbers, int count, int x)
{
int remaining_size = count;
  
int index = 0;
  
while (1){
  //printf("Remaining size: %d\n", remaining_size);
  //printf("Current index: %d\n", index);
  //printf("Number at index: %d\n\n", numbers[index]);
  
  if (x > numbers[index+remaining_size/2]){
    index = index+remaining_size/2;
    remaining_size = (remaining_size+1)/2;
  } else if (x < numbers[index+remaining_size/2]) {
    remaining_size = remaining_size/2;
  } else {
    return numbers[index+remaining_size/2];
  }
  if (remaining_size <=1){
    return numbers[index];
  }
    
 }
}
