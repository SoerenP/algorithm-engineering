#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "veb.h"

int high(int x, struct vEB *tree)
{
	return floor(x / l_square(tree->u));
}

int low(int x, struct vEB *tree)
{
	return x % l_square(tree->u);
}

int vEB_index(int x, int y, struct vEB *tree)
{
	return x * l_square(tree->u) + y;
}

int l_square(int x)
{
	return pow(2,(floor(log(x)/2)));
}

int u_square(int x)
{
	return pow(2,(ceil(log(x)/2)));
}

int vEB_min(struct vEB *tree)
{
	return tree->min;
}

int vEB_max(struct vEB *tree)
{
	return tree->max;
}

int vEB_member(struct vEB *tree, int x)
{
	if(x == tree->min || x == tree->max)
		return 1;
	else if (tree->u == 2)
		return 0;
	else return vEB_member(tree->cluster[high(x,tree)],low(x,tree)); 
}

void vEB_empty_insert(struct vEB *tree, int x)
{
	tree->min = x;
	tree->max = x;
}

void vEB_insert(struct vEB *tree, int x)
{
	if(tree->min == 0)
		vEB_empty_insert(tree,x);
	else {
		if (x < tree->min)
		{
			int temp = tree->min;
			tree->min = x;
			vEB_insert(tree,temp);
		}
		if (tree->u > 2)
		{
			if(vEB_min(tree->cluster[high(x,tree)]) == 0)
			{
				vEB_insert(tree->summary, high(x,tree));
				vEB_empty_insert(tree->cluster[high(x,tree)],low(x,tree));
			} else {
				vEB_insert(tree->cluster[high(x,tree)],low(x,tree));
			}
		}
		if(x > tree->max){
			tree->max = x;
		}
	}
}

int vEB_pred(struct vEB *tree, int x)
{
	return 0; // TO DO
}

int main(int argv, char *argc)
{
	return 0;
}
