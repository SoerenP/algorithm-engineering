#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <stdlib.h>
#include <errno.h>
#include "sorting.h"
#include <math.h>

void print_ll_matrix_ToCSV(int *sizes, int size, size_t iter, long long data[][iter], const char *filename)
{
	FILE *fp;
	int i,j;

	fp=fopen(filename, "w+");
	if(fp)
	{
		for(i = 0; i < size; i++)
		{
			fprintf(fp, "%d",sizes[i]);
			for(j = 0; j < iter; j++)
			{
				fprintf(fp, ", %lld",data[i][j]);
			}
			fprintf(fp, "\n");
		}
		fclose(fp);
	}
}

void print_f_matrix_ToCSV(int *sizes, int size, size_t iter, double data[][iter], const char *filename)
{
	FILE *fp;
	int i,j;

	fp=fopen(filename, "w+");
	if(fp)
	{
		for(i = 0; i < size; i++)
		{
			fprintf(fp, "%d", sizes[i]);
			for(j = 0; j < iter; j++)
			{
				fprintf(fp, ", %f", data[i][j]);
			}
			fprintf(fp, "\n");
		}
		fclose(fp);
	}
}

void print_ll_ToCSV(int *sizes, long long *data, const char *filename, int size)
{
	FILE *fp;
	int i;

	fp=fopen(filename, "w+");
	if(fp){
		for(i = 0; i < size; i++){
			fprintf(fp,"%d, %lld\n",sizes[i],data[i]);
		}
		fclose(fp);
	}
}

void print_f_ToCSV(int *sizes, double data[], const char *filename, int size)
{
	FILE *fp;
	int i;

	fp=fopen(filename, "w+");
	if(fp){
		for(i = 0; i < size; i++){
			fprintf(fp,"%d, %f\n",sizes[i],data[i]);
		}
		fclose(fp);
	}
}

void printArrayToFile(const char *filename, int *a, int size)
{
	FILE *fp;
	int i;
	fp=fopen(filename,"w+");
	if(fp){
		fprintf(fp,"%d ",size);	
		for(i = 0; i < size; i++)
		{
			fprintf(fp,"%d ",a[i]);
		}
	fclose(fp);
	}	
}

int *getArrayFromFile(const char *filename)
{
	int i,j;
	FILE *fp;
	fp=fopen(filename, "r+");
	if(!fp) fprintf(stderr,"Couldnt open file\n");
	else {
		/* first nr is size of file */
		fscanf(fp, "%d",&j);
	}

	/* need to open again with r+ to get it from the start */	
	int *arr = malloc(sizeof(int)*j+1);
	if(!arr){
		fprintf(stderr,"Could not alloc to array from file\n");
		exit(1);
	}

	fp=fopen(filename, "r+");

	if(!fp) fprintf(stderr,"Couldnt open file\n");
	else {
		fscanf(fp, "%d",&j); //disregard first nr, its the size
		for(i = 0; i < j; i++)
		{
			fscanf(fp, "%d",&arr[i]);
		}
	}
	//arr[j] = -1; //Escape char HACKED
	fclose(fp);
	return arr;
}



int *getRandomArray(int size)
{
	int *array = malloc(size*sizeof(int));
	if(array)
	{
		int i;
		for(i = 0; i < size; i++){
			array[i] = rand();
		}
		return array;
	} else {
		fprintf(stderr,"Failed to alloc rnd array\n");
		return NULL;
	}
}


void printArray(int *a, int size){
	int i;
	for(i = 0; i < size; i++){
		fprintf(stderr,"%d ",a[i]);
	}
	fprintf(stderr,"\n");
}

void sorted_arrays_to_files(int low, int high, const char *fileprefix)
{
	int i, size;
	int *arr;
	char filename[128];
			
	for(i = low; i <= high; i++){
		size = (int)pow(2,i);
		arr = getRandomArray(size);
		quick_sort(arr, size, sorted_order);
		if(!(arr)){
			fprintf(stderr,"sorted_arrayss_to_files failed\n");
			 exit(1);
		}
		sprintf(filename, "%s%d", fileprefix, i);
		fprintf(stderr,"filename %s, pow %d\n",filename, i);
		printArrayToFile(filename, arr, size);
		free(arr);
	}
}

