#ifndef _veb_implicit_h
#define _veb_implicit_h

struct vEBImplicit
{
  int size;
  int height;

  // arr is the array
  // B[i] is the size of the bottom-tree rooted at depth i
  // T[i] is the size of the corresponding top-tree 
  // D[i] is the depth of the root of the corresponding top-tree
  int *arr;
  int *B;
  int *T;
  int *D;
};

struct vEBImplicit* build_veb(int* sorted_arr, int N);

void fill_table(struct vEBImplicit *veb, int rec_height, int rec_depth);

int check_complete_tree(int N);

void free_veb(struct vEBImplicit *veb);


#endif
