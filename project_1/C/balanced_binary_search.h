#ifndef __bbs_h
#define __bbs_h

struct RBTree {
	struct TreeNode *root;
	struct TreeNode *nill; /* global nill (NULL) */
};

struct TreeNode {
	struct TreeNode *left; // 8 byte
	struct TreeNode *right; // 8 byte
	struct TreeNode *parent; // 8 byte
	int val; // 4 byte
	enum {red, black} color; // god knows
};

/* constructors */
struct RBTree *createRBTree(int val);
struct TreeNode *createTreeNode(int val);

/* destructors (memory cleanup) */
void RBT_free_subtree(struct TreeNode *node);
void RBT_free(struct RBTree *tree);

/* operations on RBT */
void RBT_insert(struct RBTree *tree, struct TreeNode *z);
void RBT_insert_fixup(struct RBTree *tree, struct TreeNode *z);
void RBT_rotate_left(struct RBTree *tree, struct TreeNode *x);
void RBT_rotate_right(struct RBTree *tree, struct TreeNode *x);
void RBT_transplant(struct RBTree *tree, struct TreeNode *u, struct TreeNode *v);
void RBT_delete(struct RBTree *tree, struct TreeNode *z);
void RBT_delete_fixup(struct RBTree *tree, struct TreeNode *x);
void RBT_inorder_walk(struct TreeNode *root);
int bbs_pred(int x, int *s);
int RBT_pred(struct TreeNode *root, int val, int sofar);
struct TreeNode *RBT_min(struct TreeNode *root);
struct TreeNode *RBT_max(struct TreeNode *root);
struct TreeNode *RBT_iter_search(struct TreeNode *x, int k);
struct TreeNode *RBT_successor(struct TreeNode *x);
struct TreeNode *RBT_predecessor(struct TreeNode *x); /* is not defined as pred(x) = max{y \in S | y \leq x} but as ... < x} */

/* wrapper for pred(x) operation, pred(x) = max{y \in S | y \leq x} */
int pred(int x);

/* test functions */
void RBT_test_pred();

#endif
