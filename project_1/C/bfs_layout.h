#ifndef _bfs_layout_h
#define _bfs_layout_h

void layout_bfs(int* input, int* res, int bfs_index, int sorted_offset, int sorted_size);

int bfs_pred(int x, int* bfs_array, int N, int debug);

#endif
