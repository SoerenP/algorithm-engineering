#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include "sorting.h"

/*	Function pointer:
 *	Write it: char *make_coolness(int awesome_sauce)
 *	Wrapt it: char (*make_coolness)(int awesome_sauce)
 *  Rename it: char *(*coolness_cb)(int awesome_sauce) -> en pointer til en function som tager en int, ret en char.
 *  Pointeren hedder da coolness_cb
 */

/* fail, and let OS clean up the mess */
void die(const char *message)
{
	if(errno) {
		perror(message);
	} else {
		printf("ERROR: %s\n",message);
	}

	exit(1);
}

/**
 * A classic bubble sort function that uses the
 * compare_cb to do the sorting
 */

int *bubble_sort(int *numbers, int count, compare_cb cmp)
{
	int temp = 0;
	int i = 0;
	int j = 0;
	int *target = malloc(count * sizeof(int));

	if(!target) die("Memory error.");

	memcpy(target,numbers,count * sizeof(int));

	for(i = 0; i < count; i++)
	{
		for(j = 0; j < count - 1; j++)
		{
			if(cmp(target[j],target[j+1]) > 0)
			{
				temp = target[j+1];
				target[j+1] = target[j];
				target[j] = temp;
			}
		}
	}

	return target;
}

int sorted_order(int a, int b)
{
	return a-b;
}

int reverse_order(int a, int b){
	return b-a;
}

int strange_order(int a, int b)
{
	if(a == 0 || b == 0)
	{
		return 0;
	} else {
		return a % b;
	}
}

/**
  * Insertion_Sort
  */
int *insertion_sort(int *numbers, int count, compare_cb cmp)
{
	int i,j,temp;
	int *target = malloc((sizeof(int)*count));

	if(!target) die("Memory error.");

	memcpy(target,numbers,count * sizeof(int));	

	for(i=1; i < count; i++)
	{
		j = i;
		while ((j > 0) && (target[j-1] > target[j])){
		temp = target[j];
		target[j] = target[j-1];
		target[j-1] = temp;
		j--; 
		}
	}
	return target;
}

int *quick_sort(int *numbers, int count, compare_cb cmp)
{
  quicksort_rec(numbers,0,count-1);
  return numbers;
}


//quick Sort function to Sort Integer array list
void quicksort_rec(int *array, int firstIndex, int lastIndex)
{
    //declaaring index variables
    int pivotIndex, temp, index1, index2;

    if(firstIndex < lastIndex)
    {
        //assigninh first element index as pivot element
        pivotIndex = firstIndex;
        index1 = firstIndex;
        index2 = lastIndex;

        //Sorting in Ascending order with quick sort
        while(index1 < index2)
        {
            while(array[index1] <= array[pivotIndex] && index1 < lastIndex)
            {
                index1++;
            }
            while(array[index2]>array[pivotIndex])
            {
                index2--;
            }

            if(index1<index2)
            {
                //Swapping opertation
                temp = array[index1];
                array[index1] = array[index2];
                array[index2] = temp;
            }
        }

        //At the end of first iteration, swap pivot element with index2 element
        temp = array[pivotIndex];
        array[pivotIndex] = array[index2];
        array[index2] = temp;

        //Recursive call for quick sort, with partiontioning
        quicksort_rec(array, firstIndex, index2-1);
        quicksort_rec(array, index2+1, lastIndex);
    }
}

/**
 * Used to test that we are sorting things correctly
 * by doing sorting and printing it out
 *
 */


void test_sorting(int *numbers, int count, compare_cb cmp, sorting_alg sort)
{
	int i = 0;
	int *sorted = sort(numbers, count, cmp);
	
	if(!sorted) die("Failed to sort a sequence.");

	for(i = 0; i < count; i++)
	{
		printf("%d ",sorted[i]);
	}
	printf("\n");
	free(sorted);

	unsigned char *data = (unsigned char *)cmp;
	
	for(i = 0; i < 25; i++){
		printf("%02x:",data[i]);
	}
	printf("\n");
}
