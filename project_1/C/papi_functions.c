#include "papi.h"
#include <stdlib.h>
#include <stdio.h>
#include "project1.h"
#include "papi_functions.h"
#include <string.h>
#include "naive_binary_search.h"

void test_fail(char *file, int line, char *call, int retval)
{
	fprintf(stderr,"%s\tFAILED\nLine # %d\n", file, line);
	if(retval == PAPI_ESYS){
		char buf[128];
		memset(buf, '\0', sizeof(buf));
		sprintf(buf, "System error in %s:", call);
		perror(buf);
	}
	else if ( retval > 0) {
		fprintf(stderr,"Error calculating: %s\n", call);
	} 
	else {
		fprintf(stderr,"Error in %s: %s\n", call, PAPI_strerror(retval));
	}
	fprintf(stderr,"\n");
	exit(1);
}


void testPapiNaive(int *targets, int *universe, int size)
{
	float real_time, proc_time, mflops;
	long long flpins;
	int i;
	
	int *temp = malloc(sizeof(int)*size);

	init_Time_PAPI(&real_time, &proc_time, &mflops, &flpins);

	if(!temp){
		fprintf(stderr,"failed to alloc at qeuePred\n");
		exit(1);
	} else {
		for(i = 0; i < size; i++){
			temp[i] = naive_pred(universe,size,targets[i]);
		}
	}

	init_Time_PAPI(&real_time, &proc_time, &mflops, &flpins);
	
	printf("Real_time:\t%f\nProc_time:\t%f\nTotal flpins:\t%lld\nMFLOPS:\t\t%f\n",
	real_time, proc_time, flpins, mflops);
	printf("%s\tPASSED\n", __FILE__);
	PAPI_shutdown();
	if(temp) free(temp);	
	exit(0);
}


void stop_PAPI(long long *values, int eventsize)
{
	int ret;
	if((ret = PAPI_stop_counters(values,eventsize)) != PAPI_OK) test_fail(__FILE__,__LINE__,"PAPI_stop_counters", ret);
}

void init_PAPI(int *events, int eventsize)
{
		if(PAPI_num_counters() < eventsize) {
		fprintf(stderr, "No hardware counters here, or PAPI not supportned\n");
	exit(1);
	}
	int ret;	
	if((ret = PAPI_start_counters(events,eventsize)) != PAPI_OK) test_fail(__FILE__,__LINE__,"PAPI_start_counters", ret);
}


void init_Time_PAPI(float *real_time, float *proc_time, float *mflops, long long *flpins)
{
	int retval;
	if((retval=PAPI_flops(real_time, proc_time, flpins, mflops))<PAPI_OK)
		test_fail(__FILE__,__LINE__,"PAPI_flops", retval);
}



void pred_PAPI(int *targets, int *universe, int testsize, PredQuery prec, long long *values, int *events, int eventsize)
{
	/* for events to papi, look at http://icl.cs.utk.edu/projects/papi/wiki/PAPIC:PAPI_presets.3 */	

	init_PAPI(events, eventsize);

	int *res;
	res = prec(targets, universe, testsize);

	stop_PAPI(values, eventsize);
	PAPI_shutdown();
	if(res) free(res); 
}

float time_pred(int *targets, int *universe, int testsize, PredQuery pred)
{
	float real_time, proc_time, mflops;
	long long flpins;
	int *res;
	init_Time_PAPI(&real_time, &proc_time, &mflops, &flpins);
	res = pred(targets, universe, testsize);
	init_Time_PAPI(&real_time, &proc_time, &mflops, &flpins);
	if(res) free(res);
	return real_time;
}

float Pred_Naive_time_PAPI(int targets[], int targetsize, int *universe, int unisize)
{
	float real_time, proc_time, mflops;
	long long flpins;
	init_Time_PAPI(&real_time, &proc_time, &mflops, &flpins);
	int i;
	for(i = 0; i < targetsize; i++){
		naive_pred(universe,unisize,targets[i]);
	}
	init_Time_PAPI(&real_time, &proc_time, &mflops, &flpins);
	printf("real_time %f\n",real_time);
	return real_time;
}

