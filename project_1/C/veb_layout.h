#ifndef _veb_layout_h
#define _veb_layout_h

void layout_veb(struct vEBImplicit *veb);

void layout_veb_rec(int* input, int* res, int veb_index, int bfs_offset, int veb_size);

int veb_layout_pred(int x, struct vEBImplicit *veb);

#endif
