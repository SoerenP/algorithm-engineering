#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include "dbg.h"
#include "naive_binary_search.h"
#include "sorting.h"
#include "dfs_layout.h"

int N = 10012;
int M = 100000;
int runs = 100000;

int main(int argc, char **argv){

  srand(time(NULL));

  int *array = malloc(N*sizeof(int));
  int i;
  for (i = 0; i < N; i++){
    array[i] = rand() % M;
  }

  int* sorted_array = insertion_sort(array,N,sorted_order);

  int* dfs_array = malloc(N * sizeof(int));
  int* left_tree_sizes = malloc(N * sizeof(int));
  layout_dfs(sorted_array, dfs_array, left_tree_sizes, 0, 0, N);

  int disagreements = 0;

  for (i = 0; i < runs; i++){
    int target = rand() %(M);
    //printf("Target is %d\n", target);
    int naive = naive_pred(sorted_array,N,target);
    
    int dfs_res = dfs_pred(target, dfs_array, left_tree_sizes, N, 0);
    //int dfs_res = 0;
    if (naive == dfs_res){
      //printf("naive and veb agreed on %d, which they both got to %d\n", target, naive);
    } else {
      dfs_pred(target,dfs_array, left_tree_sizes,N,1);
      printf("naive and dfs DISAGREED on %d, where naive got %d and dfs got %d\n", target,naive,dfs_res);
      disagreements++;
    }    
  }
  printf("The number of disagreements is %d\n\n", disagreements);
  
  /*  printf("Sorted order:\n");
  for (i = 0; i < N; i++){
    printf("%d, ", sorted_array[i]);
  }
  printf("\n\n");

  printf("Dfs order:\n");
  for (i = 0; i < N; i++){
    printf("%d, ", dfs_array[i]);
  }
  printf("\n\n");

  printf("Sizes of subtrees:\n");
  for (i = 0; i < N; i++){
    printf("%d, ", left_tree_sizes[i]);
  }
  printf("\n");*/
  
  return 1;
}
