#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "veb_implicit.h"
#include "veb_layout.h"
#include "bfs_layout.h"


void layout_veb(struct vEBImplicit *veb){

  int* bfs = malloc(sizeof(int)*veb->size);
  layout_bfs(veb->arr, bfs, 1, 0, veb->size);
  layout_veb_rec(bfs, veb->arr, 0, 1, veb->size);
  //free(bfs);
}

void layout_veb_rec(int* input, int* res, int veb_index, int bfs_offset, int veb_size){

  int height = (int) ceil(log2((double)(veb_size+1)));
  
  if (veb_size > 1){
    int bot_height = height/2;
    int bot_size = (int) pow(2.0,(double)bot_height) - 1;
    int top_height = height-bot_height;
    int top_size = (int) pow(2.0,(double)top_height) - 1;
    int bot_trees = pow(2.0,(double)(top_height));
    layout_veb_rec(input,res,veb_index,bfs_offset, top_size);
    int i;
    for (i = 0; i < bot_trees; i++){
      layout_veb_rec(input,res,veb_index+top_size+i*bot_size, bfs_offset*bot_trees+i, bot_size);
    } 
  } else {
    res[veb_index] = input[bfs_offset-1];
  }
  

}

int veb_layout_pred(int x, struct vEBImplicit *veb){

  int d = 0;
  int* pos = malloc(veb->height*sizeof(int));
  pos[0] = 0;
  int bfs_index = 1;

  for (d = 0; d < veb->height; d++){
    pos[d] = 
      pos[veb->D[d]]             // The position of the corresponding top tree
      + (veb->T[d])              // The size of the corresponding top tree
      + (bfs_index & (veb->T[d]))// The T[d] least significant bits of the bfs position
      * veb->B[d];               // The size of this bottom tree
    //printf("Position in veb was %d at depth %d\n", pos[d], d);
    //printf("T[d] is %d, D[d] is %d, bfs_index is %d, pos[D[d]] is %d\n", veb->T[d], veb->D[d], bfs_index, pos[veb->D[d]]);
    //printf("Compared %d to target: %d\n", veb->arr[pos[d]], x);
    if (veb->arr[pos[d]] > x){
      // Go left
      //printf("Went left\n");
      bfs_index = bfs_index*2;
    } else if (veb->arr[pos[d]] < x){
      // Go right
      //printf("Went right\n");
      bfs_index = bfs_index*2+1;
    } else if (veb->arr[pos[d]] == x){
      return x;
    }
    
  }
  d = d-1;
  // If we didn't find the input, get the predecessor based on the following if: 
  //printf("Reached the end of the tree, comparing target: %d to %d at depth %d\n", x, veb->arr[pos[d]],d);
  if (veb->arr[pos[d]] > x){
    while (x < veb->arr[pos[d]] && d >= 0){
      d = d-1;
    }
    if (d >= 0)
      return veb->arr[pos[d]];
    return -1;
  } 
  return veb->arr[pos[d]];
  
}

