#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "bfs_layout.h"
#define MAX(X, Y) (((X) > (Y)) ? (X) : (Y))
#define MIN(X, Y) (((X) < (Y)) ? (X) : (Y))


void layout_bfs(int* input, int* res, int bfs_index, int sorted_offset, int sorted_size){

  // This is the height up to where the tree can be filled completely
  int height = (int)floor(log2((double)(sorted_size+1)));

  // This is the minimum size of the two trees below this root
  int left_size = (int)pow(2.0,(double)(height-1))-1;
  int right_size = left_size;
  
  // This is the remaining number of nodes
  int rest = sorted_size-1-left_size-right_size;

  int next_left_layer_size = (int)pow(2.0, (double)(height-1)); 
  if (rest > next_left_layer_size){
    // If there are enough numbers left to fill a layer in the left tree, do so and add the rest to the right tree
    rest = rest - next_left_layer_size;
    left_size = left_size + next_left_layer_size;
    right_size = right_size + rest;
  } else if (rest <= next_left_layer_size){
    // If there aren't enough numbers left to fill an entire layer, then just add them to the left tree
    left_size = left_size + rest;
  }

  //printf("Size of left was %d and size of right was %d and sorted size was %d\n", left_size, right_size, sorted_size);
  // Set the root node in this tree
  res[bfs_index-1] = input[sorted_offset+left_size];
  //printf("Sorted offset was %d\n", sorted_offset);

  //printf("Set the value at bfs_index %d to %d from the %d'th index of sorted order\n", bfs_index-1, res[bfs_index-1], sorted_offset+left_size);
  // If there are nodes in the left or right trees, recursively layout them
  if (left_size > 0){
    layout_bfs(input,res,bfs_index*2,sorted_offset,left_size);
  }
  if(right_size > 0){
    layout_bfs(input,res,bfs_index*2 +1, sorted_offset+left_size+1, right_size);
  }
}


int bfs_pred(int x, int* bfs_array, int N, int debug){

  int bfs_index = 1;
  int current_best = -1;
  int height = (int)ceil(log2((double)N));
  int i;
  for (i = 0; i < height && bfs_index <= N; i++){
    if (debug) printf("bfs index is %d\n", bfs_index);
    if (debug) printf("Compared %d to target: %d\n", bfs_array[bfs_index-1], x);
    if (bfs_array[bfs_index-1] > x){
      if(debug) printf("Went left\n");
      bfs_index = bfs_index*2;
    } else if (bfs_array[bfs_index-1] < x){
      if (debug) printf("Went right\n");
      if (current_best < bfs_array[bfs_index-1]){
	current_best = bfs_array[bfs_index-1];
      }
      bfs_index = bfs_index*2 +1;
    } else if (bfs_array[bfs_index-1] == x){
      return x;
    }
  }

  return current_best;


}
