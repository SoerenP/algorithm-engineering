#include "bitmap.h"
#include "clark_select.h"
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#define C_VAL 7

int clark_select(int index, struct Bitmap* bitmap, struct Level1Dir* level1){
  
  int n = bitmap->bits;
  int num_ones_in_level_1_subrange = ceil(log2((double)n)) * ceil(log2(log2((double)n)));

  // i is the starting point
  int i = ((index-1)/num_ones_in_level_1_subrange);


  struct Level2Dir* level2 = &(level1->subdir[i]);
  //printf("First access\n");
  int ones_accounted_for_at_level_1 = i*num_ones_in_level_1_subrange;
  int position_from_level_1 = level2->pos; 
      
  /* r is the size of the subrange, which we want to search in. */
  int r = n - level2->pos;
  if (i < level1->size){
    r = level1->subdir[i+1].pos-level2->pos;
  } 
  if (r >= num_ones_in_level_1_subrange*num_ones_in_level_1_subrange)
    {
      /* If this is the case, we have all the answers explicitly recorded in the level two dir, i.e. the position of the i'th 1 in the substring is at level2->subdir[i]->pos. We lookup in the explicit answers using the remainder of our index as the input. */
      
      //printf("Had enough space for explicit storage\n");
      int position_from_level_2 = level2->subdir[(index-1) - ones_accounted_for_at_level_1].pos;
      return position_from_level_1 + position_from_level_2;
    } 
  else 
    {
      /* If r is less than pow(ceil(log2(n))*ceil(log2(log2(n))),2) then the level two dir contains a bunch of subdirs, which we need to search through. */
			//printf("Has to search through level 2 subranges\n");
      int num_ones_in_level_2_subrange = (int)(ceil(log2((double)r))*ceil(log2(log2((double)n))));
      //printf("r is %d\n", r);
      //printf("Number of ones in a level 1 subrange: %d, number of ones in a level 2 subrange: %d\n", num_ones_in_level_1_subrange, num_ones_in_level_2_subrange);
      int j = ((index-1)-ones_accounted_for_at_level_1)/num_ones_in_level_2_subrange;

      struct Level3Dir* level3 = &(level2->subdir[j]);

      int ones_accounted_for_at_level_2 = j*num_ones_in_level_2_subrange;

      int position_from_level_2 = level3->pos;
      
      /* r_mark is the size of the subrange in level2, which we want to search through */
      //printf("j %d\n",j);
      int r_mark = 0;
      if (j >= num_ones_in_level_1_subrange/num_ones_in_level_2_subrange){
	r_mark = r - level3->pos;
      } else {
	r_mark = level2->subdir[j+1].pos - level3->pos;
      }
      if (r_mark >= ceil(log2((double)r_mark))*ceil(log2((double)r))*pow(ceil(log2(log2((double)n))),2.0))
	{
	  /* If we have enough space for explicit storage, we simple look up under the right index and get the remainder of the position */
	  //printf("Had enough space in level 3\n");
	  //printf("Ones accounted for at level 1 is %d and at level 2 it is %d and ones per subrange in level 1 is %d\n", ones_accounted_for_at_level_1, ones_accounted_for_at_level_2, num_ones_in_level_1_subrange);
	  int position_from_level_3 = 
	    level3->subpos[(index-1) 
			   - ones_accounted_for_at_level_1 
			   - ones_accounted_for_at_level_2];
	  //printf("After subpos\n");
	  return position_from_level_1 
	    + position_from_level_2 
	    + position_from_level_3;
	} 
      else 
	{
	  /* If we STILL need more space, we need to do a constant time search (lookup in an array with answers for bitstring B and select i) on the bitstring represented by the level 3 directory */
	  int ones_not_accounted_for = index 
	    - ones_accounted_for_at_level_1 
	    - ones_accounted_for_at_level_2;
	  //printf("Ones accounted for at level 1 is %d and at level 2 it is %d\n", ones_accounted_for_at_level_1, ones_accounted_for_at_level_2);
	  int ones_accounted_for_at_level_3 = 0;
	  int substring_size = (int)log2((double)n)/C_VAL;
	  if (!substring_size) substring_size = 1;
	  //printf("Substring size is %d\n",substring_size);
	  int position_from_level_3 = 0;
	  int start_bit = position_from_level_1 + position_from_level_2;
	  //printf("start bit is %d, index in level 1 is %d and fom level 2 it is %d\n", start_bit, position_from_level_1, position_from_level_2);
	  int end_bit = start_bit + substring_size-1;
	  int current_bitstring = fromBitmapToInt32Const(bitmap,
						    start_bit, 
						    end_bit);
		while (ones_not_accounted_for > level3->num_ones[current_bitstring])
	    {
	    	//printf("Ones not accounted for is %d\n", ones_not_accounted_for);
	    	//printf("Current bitstring %d for level 3 with position %d. The number of ones was %d\n", current_bitstring, level3->pos, level3->num_ones[current_bitstring]);
		position_from_level_3 += substring_size;
	      ones_not_accounted_for -= level3->num_ones[current_bitstring];
	      ones_accounted_for_at_level_3 += level3->num_ones[current_bitstring];
	      start_bit = end_bit+1;
	      end_bit = start_bit+substring_size-1;
	      if (end_bit >= n) end_bit = n-1;
	      current_bitstring = fromBitmapToInt32Const(bitmap,start_bit,end_bit);
	    }
	      
		//printf("current bitstring %d ones not accounted for %d\n", current_bitstring, ones_not_accounted_for);
	  int final_position = start_bit
	    + level3->pos_of_ones[current_bitstring][ones_not_accounted_for-1];
	      
	  return final_position;
	}		    
	    
    }

  return -1;
}


/*
 * Level 1 of the auxilliary dirs is a directory containing the location of 
 * every lg n lg lg n th 1 bit (rounded up). An entry i in this directory
 * also contains a pointer to the level 2 directory representing the substring 
 * between the dir[i-1]'th and dir[i]'th ones.
 */
struct Level1Dir* build_level_1(struct Bitmap* bitmap){
  
  struct Level1Dir* level_1 = malloc(sizeof(struct Level1Dir));

  int n = bitmap->bits;
  //printf("n is %d\n",n);
  int lgn = (int) ceil(log2((double)n));
  int lglgn = (int) ceil(log2(log2((double)n)));
  int num_ones_in_level_1_subrange = lgn * lglgn;
  // Make sure that there is enough space, even if all the bits are 1
  level_1->subdir = malloc(sizeof(struct Level2Dir) * (int)ceil(((double)n)/((double)num_ones_in_level_1_subrange)));

  int one_counter = 0;
  int r = 1;
  int level_2_counter = 0;
  int i;

  /* Scan through the string, and make a level 2 subdir for every lg n lg lg n 'th one-bit discovered */ 
  for (i = 0; i < n; i++,r++)
    {

      
      if (test_bitmap(bitmap,i)) one_counter++;
      if (one_counter >= num_ones_in_level_1_subrange)
	{
	  one_counter = 0;
	  build_level_2(bitmap,&level_1->subdir[level_2_counter],r,i-r+1, lgn, lglgn);
	  level_2_counter++;
	  r = 0;
	}
	
    }
  if (r > 0)
  {
    build_level_2(bitmap,&level_1->subdir[level_2_counter++],r,i-r+1, lgn, lglgn);
  }  
  level_1->size = level_2_counter;
  return level_1;
}

/*
 * A level 2 dir has two cases:
 * The first is if there is enough space available (this space is 
 * based on the size of the array, because the entire structure is
 * bounded in it's use of memory) to explicitly represent 
 * the position of each 1 bit in the substring, which we then do.
 * The second is if there isn't enough space, in which case we have to 
 * keep track of each lg size lg lg n (rounded up) 'th 1-bit, and then use
 * level 3 dirs to keep track of the substring between two entries.
 */
void build_level_2(struct Bitmap* bitmap, struct Level2Dir* level_2, int r, int pos, int lgn, int lglgn){
  
  //level_2->size = r;
  level_2->pos = pos;
  
  int num_ones_in_level_1_subrange = lgn * lglgn;
  if (r >= num_ones_in_level_1_subrange * num_ones_in_level_1_subrange)
    {

      /* Explicitly represent the position of the i'th 1-bit on pos i */ 
      level_2->subdir = malloc(sizeof(struct Level3Dir)*num_ones_in_level_1_subrange);
      //level_2->subdir_size = num_ones_in_level_1_subrange;
      int i,j=0;
      for (i = 0; i < r; i++)
	{
	  if (test_bitmap(bitmap,i))
	    level_2->subdir[j++].pos = i;
	}

    } 
  else 
    {
      /* Make a level 3 subdir for every entry in the level 2 dir */
      //																 (int)(ceil(log2((double)r))*ceil(log2(log2((double)n))))
      int lgr = (int)ceil(log2((double)r));
      int num_ones_in_level_2_subrange = lgr*lglgn;
      int num_subranges_level_2 = (int)ceil((double)num_ones_in_level_1_subrange/(double)num_ones_in_level_2_subrange)+1;
      //printf("r %d, num_subranges_level_2 %d, num_ones_in_level_2_subrange %d\n",r,num_subranges_level_2,num_ones_in_level_2_subrange);
      int malloc_size = sizeof(struct Level3Dir)*num_subranges_level_2;
      //printf("Allocating %d memory for level 2s subdirs\n",malloc_size);
      level_2->subdir = malloc(malloc_size);
      //printf("Done allocating\n");
      //level_2->subdir_size = num_subranges_level_2;
      int i,r_mark=1,one_counter=0,level_3_counter=0;
      for (i = 0; i < r; i++,r_mark++) 
	{
	  
	  //printf("pos %d, i %d\n, r %d\n",pos,i,r);
	  if (test_bitmap(bitmap,pos+i))
	    {
	      one_counter++;
	      if (one_counter >= num_ones_in_level_2_subrange)
		{
			//printf("built a lvl 3 subdir\n");
		  build_level_3(bitmap, &level_2->subdir[level_3_counter], r_mark, r, i-r_mark+1, num_ones_in_level_2_subrange, lgn, lglgn, lgr);
		  one_counter = 0;
		  level_3_counter++;
		  r_mark = 0;
		}
	    } 
	}
      if (r_mark > 0)
      {
      	//printf("set index %d with size %d\n", level_3_counter,level_3_size);
	build_level_3(bitmap, &level_2->subdir[level_3_counter], r_mark, r, i-r_mark+1, num_ones_in_level_2_subrange, lgn, lglgn, lgr); 
			}
    }
}

/*
 * A level 3 dir has two cases:
 * The first is if there is enough space available (this space is 
 * based on the size of the array, because the entire structure is
 * bounded in it's use of memory) to explicitly represent 
 * the position of each 1 bit in the substring, which we then do.
 * The second is if there isn't enough space, in which case we want to
 * make a two-dimensional array with the position of the i'th 1 bit for
 * every possible bitstring of the given length. 
 */
void build_level_3(struct Bitmap* bitmap, struct Level3Dir* level_3, int r_mark, int r, int pos, int num_ones_in_level_2_subdir, int lgn, int lglgn, int lgr){

  int n = bitmap->bits;

  level_3->pos = pos;
  level_3->subpos = NULL;
  level_3->num_ones = NULL;
  level_3->pos_of_ones = NULL;
  if (r_mark >= (int)ceil(log2((double)r_mark))*lgr*lglgn*lglgn)
    {
      /* Explicitly record all answers */
      level_3->subpos = malloc(num_ones_in_level_2_subdir*sizeof(int));
      int i,j=0;
      for (i = 0; i < r_mark; i++)
	{
	  if (test_bitmap(bitmap,pos+i))
	    level_3->subpos[j++] = i;
	}
    } 
  else 
    {
      /* Store all the possible answers to bitstrings of length lg n / c 
	 so it's possible to scan */
      int subrange_size = (int)(log2((double)n)/C_VAL);
      if (!subrange_size) subrange_size = 1;
      //int possible_subranges = (int)pow(2.0,(double)subrange_size);
      int possible_subranges = 1 << subrange_size;
      //printf("Possible subranges %d\n",possible_subranges);
      level_3->pos_of_ones = malloc(sizeof(int*)*possible_subranges);
      level_3->num_ones = malloc(sizeof(int)*possible_subranges);
      int i;
      for (i = 0; i < possible_subranges; i++)
	{
		//printf("Made the solutions for bitstring %d for level 3 with pos %d\n", i, level_3->pos);
	  level_3->num_ones[i] = count_ones(i);
	  level_3->pos_of_ones[i] = find_pos_of_ones(i,subrange_size);
	} 
    
    }
}
  
int count_ones(unsigned int bitstring){
  unsigned int res = 0;
  while (bitstring > 0){
    res += bitstring % 2;
    bitstring = bitstring >> 1;
  }
  return res;
}

int* find_pos_of_ones(unsigned int bitstring, int size){
  int* res = malloc(size*sizeof(int));
  int head = 0;
  int i = 0;
  while (bitstring > 0){
    //i++;
    if (bitstring%2)
    {
    	//printf("Set entry for bitstring %d and index %d to position %d\n", bitstring, head, i);	
      res[head++] = i;
    }
    i++;
    bitstring = bitstring >> 1;
  }
  for (; head < size; head++){
    res[head] = -1;
  }
  return res;
}


void free_dirs(struct Level1Dir* level1, int n){
  int i,j,k;
  int lglgn = (int) ceil(log2(log2((double)n)));
  double lgn = log2((double)n);
  int num_ones_in_level_1_subrange = (int)ceil(lgn) * lglgn;
  for (i = 0; i < level1->size; i++){
    struct Level2Dir* level2 = &(level1->subdir[i]);
    int r = 0;
    if (i < level1->size)
      {
	r = level1->subdir[i+1].pos - level2->pos;
      } 
    else 
      {
	r = n - level2->pos;
      }
    int num_ones_in_level_2_subrange = (int)(ceil(log2((double)r))*lglgn);
    for (j = 0; j < (int)ceil((double)num_ones_in_level_1_subrange/(double)num_ones_in_level_2_subrange); j++){
      struct Level3Dir* level3 = &(level2->subdir[j]);
      
      if (level3->subpos){
	free(level3->subpos);
      } else {
	
	int subrange_size = (int)(lgn/C_VAL);
	if (!subrange_size) subrange_size = 1;
	//int possible_subranges = (int)pow(2.0,(double)subrange_size);
	int possible_subranges = 1 << subrange_size;
	for (k = 0; k < possible_subranges; k++){
	  
	  free(level3->pos_of_ones[k]);
	}
	free(level3->num_ones);
	free(level3->pos_of_ones);
      }
    }
    free(level2->subdir);
  }
  free(level1->subdir);
  free(level1);
}
