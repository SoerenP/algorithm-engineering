#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include "implicit_succint.h"
#include "papi_wrap.h"
#include "bitmap.h"
#include "file_io.h"
#include "papi.h"

#define tid 0
#define l1 1
#define l2 2
#define tlb 3
#define branch 4
#define instr 5

int *getPapiEvents(int request){
	int *res = malloc(sizeof(int));
	
	switch(request)
	{
		case l1:
			res[0] = PAPI_L1_DCM;
			break;
		case l2:
			res[0] = PAPI_L2_DCM;
			break;
		case tlb:
			res[0] = PAPI_TLB_TL;
			break;
		case branch:
			res[0] = PAPI_BR_MSP;
			break;
		case instr:
			res[0] = PAPI_TOT_INS;
			break;
		default:
			res[0] = PAPI_L1_DCM;
			break;
	}
	return res;
}

void impl_succ_rank_papi
(struct impl_succ *is, size_t queries, int q[queries], size_t n, size_t m, int R_p[n][m],long long *values, int *events, int eventsize)
{
	/* setup */
	int i,res;
	
	/* start counting */
	init_PAPI(events, eventsize);
	
	/* do work */
	for(i=0; i < queries; i++)
	{
		res = implicit_succint_rank(is, q[i], n, m, R_p);
	}
	res++;
	/* stop counting */
	stop_PAPI(values, eventsize);
	
	/* clean up */

}

double time_impl_succ_rank(struct impl_succ *is, size_t queries, int q[queries], size_t n, size_t m, int R_p[n][m])
{
	/* setup */
	clock_t begin, end;
	double time_spent;
	int i, res;
	
	/* start counting */
	begin = clock();
	
		/* do work */
	for(i=1; i < queries; i++)
	{
		res = implicit_succint_rank(is, q[i], n, m, R_p);
	}
	
	/* stop counting */
	end = clock();
	time_spent = (double)(end-begin) / CLOCKS_PER_SEC;
	res++;
	/* cleanup */
	return time_spent;
}

int main(int argc, char **argv)
{
	// seed randomizer, ONLY ONCE!
	srand(time(NULL));

	if(argc < 2){
		printf("Usage: %s <bitmap size low> <bitmap size high> <queries> <rank/select> <iter> <data> <resultfile>\n",argv[0]);
		printf("Example: %s 10 20 10000 rank 10 1 test_results/time/example.csv\n",argv[0]);
		printf("Data can be 0 = tid, 1 = l1, 2 = l2, 3 = tlb, 4 = branch, 5 = instr\n");
		return 0;
	} else if(argc != 8){
		printf("Not enough arguments\n");
		return 0;
	}
	
	/* get values from input */
	int low_size = atoi(argv[1]);
	int high_size = atoi(argv[2]);
	int queries = atoi(argv[3]);
	int rank;
	int data = atoi(argv[6]);
	char *filename = argv[7];
	int j,k;
	int iter = atoi(argv[5]);
	
	/* make arrays for data gathering */
	double times[high_size - low_size][iter];
	int sizes[high_size - low_size];
	long long values[2]; // Array for papi results
	long long int res[high_size-low_size+1][iter];
	int *events = getPapiEvents(data);

	if(strcmp(argv[4],"rank") == 0) rank = 1;
	else rank = 0;
	printf("%s %d %d %d %d %d %s\n",argv[0],low_size,high_size,queries, rank, data, filename);


	/* collect data from size high to low */
	for(j = low_size, k = 0; j <= high_size; j++, k++)
	{
		printf("\n\nSIZE 2^%d\n",j);
		int size = (int)pow(2,j);
		// Setup bitmap af ønskede størrelse,og udfyld med random bits
		struct Bitmap *bm = create_bitmap(size+1);
		int blocksize = length_b(size);
		int superblocksize = length_s(size);
		int block_count = ceil((float)size/(float)blocksize);
		int superblock_count = ceil((float)size/(float)superblocksize);
		int permutations = (int)pow(2,blocksize);
		printf("Data...\n");
		printf("\tBitmap[1..%d]\n",size);
		printf("\tblocksize: \t\t%d \tbits\n",blocksize);
		printf("\tsuperblocksize: \t%d \tbits\n",superblocksize);
		printf("\tblock_count: \t\t%d \tblocks\n",block_count);
		printf("\tsuperblock_count: \t%d \tsuperblocks\n",superblock_count);
	
		int R_p[permutations][blocksize+1];	//OBS blocksize +1
		
		int s,i,n_ones, rank;
		//Array for precomputation of ranks. 
		int *precomputed = malloc(sizeof(int)*(blocksize*block_count+1));
		
		
		n_ones = 0, rank = 0;
		printf("Setup...\n");
		
		printf("\tSetting Random Bits...\n");
		
		for(i = 1; i <= size; i++)
		{
			if((rand() % 2) > 0)
			{
				set_bitmap(bm,i);
				n_ones++, rank++;
			}
			precomputed[i] = rank;
		}
		
		printf("\tCreating R_p...\n");
		
		for(s = 0; s < permutations; s++)
		{
			struct Bitmap *temp = fromIntToBitmap(s,blocksize);
			for(i = 0; i <= blocksize; i++)
			{
				R_p[s][i] = naive_rank_bitmap(temp,i);
			}
			free_bitmap(temp);
		}
		
		struct impl_succ *is = create_impl_succ(blocksize, superblocksize, bm);
		//setup_impl_succ(is);
		setup_impl_succ_precomputed(is,precomputed);
		
		// Setup for measurements
		printf("Setup Queries...\n");
		int q[queries];
		for(i = 0; i < queries; i++)
		{
			q[i] = rand() % size; //make sure queries are in our range
		}
	
		printf("Querying...\n");
		printf("\titer...");
		double time_spent;
		//Either we gather time;
		if(data == 0){
			for(i=0; i < iter; i++)
			{
				time_spent = time_impl_succ_rank(is,queries,q,permutations,blocksize+1,R_p);
				printf("%d..",i);
				times[k][i] = time_spent;
			}
		} 
		else //Or we gather papi
		{
			for(i=0; i < iter; i++)
			{
				impl_succ_rank_papi(is,queries,q, permutations, blocksize+1,R_p,values, events, 1);
				printf("%d..",i);
				res[k][i] = values[0];
			}
		}
		printf("\n");
		sizes[k] = j;
		
		free(precomputed);
		free_impl_succ(is);
		free_bitmap(bm);
	}

	// Skriv res til fil
	if(data==0)
		print_f_matrix_ToCSV(sizes, high_size-low_size+1, iter, times, filename);
	else
		print_ll_matrix_ToCSV(sizes, high_size-low_size+1, iter, res, filename);
	// Ryd op
	

	return 0;
}
