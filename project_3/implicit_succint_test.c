#include "bitmap.h"
#include "compact.h"
#include "implicit_succint.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <string.h>

int main(int argc, char **argv){
	
	// seed randomizer, ONLY ONCE!
	srand(time(NULL));

	// stuff for bitmap
	if(argc < 2){
		printf("Usage: implicit <bitvector size> <num. of rank queries>\n");
		return 0;
	}

	int size = atoi(argv[1]);
	int queries = atoi(argv[2]);	

	struct Bitmap *bm = create_bitmap(size+1); // skal den ha size + 1? vi bruger jo ikke 0'te bit
	int blocksize = length_b(size);
	int superblocksize = length_s(size);
	int block_count = ceil((float)size/(float)blocksize);
	int superblock_count = ceil((float)size/(float)superblocksize);
	int permutations = (int)pow(2,blocksize);	
	printf("Data...\n");	
	printf("\tBitmap[1..%d]\n",size);
	printf("\tblocksize: \t\t%d \tbits\n",blocksize);
	printf("\tsuperblocksize: \t%d \tbits\n",superblocksize);
	printf("\tblock_count: \t\t%d \tblocks\n",block_count);
	printf("\tsuperblock_count: \t%d \tsuperblocks\n",superblock_count);

	int R_p[permutations][blocksize+1];	//OBS blocksize +1
	
	int s,i,n_ones, rank;
	int *precomputed = malloc(sizeof(int)*(superblocksize*superblock_count+1));
	n_ones = 0, rank = 0;
	printf("Setup...\n");
	printf("\tSetting Random Bits...\n");
	// sæt random bits i bitmap
	// TODO Hvis du bruger random_bitmap skal du sætte den 0'te bit til 0 for at være sikker, da select naive kører fra 0'te bit, men 
	// select som bygger ovenpå rank er 1 indexeret!! 
	for(i = 1; i <= size; i++)
	{
		if((rand() % 2) > 0)
		{
			set_bitmap(bm,i);
			n_ones++;
			rank++;
		}
		precomputed[i] = rank;
	}
	//print_bitmap(bm);
	// for each bit stream S af længde b (2^b mulige) og for hver position i inde i S (b positioner)
	// udregner vi R_p[s,i] = rank(S,i)
  //
	
	//printf("\n");
	printf("\tCreating R_p...\n");
	for(s = 0; s < permutations; s++)
	{
		
		struct Bitmap *temp = fromIntToBitmap(s,blocksize);
		//print_bitmap(temp);
		
		for(i = 0; i <= blocksize; i++)
		{
			R_p[s][i] = naive_rank_bitmap(temp,i);
		}
		//printf("\n");
		free_bitmap(temp);
	}
	
	int q[queries];
	for(i = 0; i < queries; i++)
	{
		q[i] = rand() % size; //make sure queries are within our range
	}
	
	//TODO gem bitvector og R_s R_b i fil istf at udregne det her hver gang? det er alligevel statisk.. 
	struct impl_succ *is = create_impl_succ(blocksize, superblocksize, bm);
	//setup_impl_succ(is);
	setup_impl_succ_precomputed(is,precomputed);
	
	printf("Querying Rank...\n");
	int res1,res2, res3, disagree;
	disagree = 0;
	

	for(i=1; i < queries; i++)
	{
		res1 = implicit_succint_rank(is, q[i],permutations,blocksize+1, R_p); //OBS blocksize+1!!!
		//printf("rank(B,%i)= %d\t",i,res1);
		res2 = naive_rank_bitmap(bm, q[i]);
		//printf("naive(B,%i)= %d\n",i,res2);
		res3 = naive_rank_bitmap_subrange(bm, 1, i);
		if(!(res1 == res2) && !(res3 == res2)){
			 disagree++;
			printf("i: %d, succ %d, naiv %d, subrange %d\n",i,res1,res2,res3);
		}
	}
	
	printf("\t Rank: Scan vs. Implicit => # disagreements = %d\n",disagree);
	
	/*printf("Querying Select...\n");
	disagree = 0;
	for(i=1; i <= n_ones; i++)
	{
		res1 = implicit_succint_select(is,i,size,permutations,blocksize+1,R_p);
		res2 = naive_select_bitmap(bm, i);
		if(!(res1 == res2))
		{ 
			disagree++;
			printf("i: %d, succ %d, naiv %d\n",i,res1,res2);
		}
	}
	printf("\t Select: Scan vs. Implicit => # disagreements = %d\n",disagree);
	*/
	free(precomputed);
	free_impl_succ(is);
	free_bitmap(bm);
	
	return 0;
}
