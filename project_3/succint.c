#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "succint.h"
#include "compact.h"
#include "bitmap.h"

struct Block *create_Block(int size)
{
	struct Block *b = malloc(sizeof(struct Block));	
	struct Bitmap *bm = create_bitmap(size);
	b->bm = bm;
	b->size = size;
	return b;
}

void free_block(struct Block *b)
{
	if(b)
	{
		if(b->bm)
		{
			free_bitmap(b->bm);
		}
		free(b);
	}
}

int calc_R_b(struct Block *b)
{
	return naive_rank_bitmap(b->bm, b->size);
}


struct SuperBlock *create_SuperBlock(int size_sb, int size_b){
	struct SuperBlock *sb = malloc(sizeof(struct SuperBlock));
	sb->b = (struct Block **) malloc(size_sb*sizeof(struct Block*));
	int i;
	for(i = 0; i < size_sb; i++)
	{
		sb->b[i] = (struct Block *)create_Block(size_b);
	}
	sb->size=size_sb;
	sb->R_b = malloc(sizeof(int)*size_sb);

	sb->R_b[0] = 0; //by definition
	for(i=1; i < size_sb; i++)
	{
		sb->R_b[i] = calc_R_b(sb->b[i-1]);
	}
	return sb;
}

void free_SuperBlock(struct SuperBlock *sb)
{
	int i;
	for(i = 0; i < sb->size; i++)
	{
		if(sb->b[i]) free_block(sb->b[i]);
	}
	free(sb->b);
	free(sb->R_b);
	free(sb);
}

struct RRR *create_RRR(int size_rrr, int size_sb, int size_b)
{
	struct RRR *r = malloc(sizeof(struct RRR));
	r->s_blocks = (struct SuperBlock **) malloc(size_rrr*sizeof(struct SuperBlock *));
	int i;
	for(i = 0; i < size_rrr; i++)
	{
		r->s_blocks[i] = (struct SuperBlock *)create_SuperBlock(size_sb,size_b);
	}
	r->size = size_rrr;
	r->R_s = malloc(sizeof(int)*size_rrr);
	r->R_s[0] = 0; //by definition
	for(i=1; i < size_rrr; ++)
	{
		r->R_s[i] = calc_R_s(r->s_blocks[i]);
	}


}
/*
int calc_R_s(struct RRR *r){
	//Find højremest block i underliggende superblocke
	struct SuperBlock *sb = r->s_blocks[r->size-1];
	//Tag dens tal og lig rank af dennes sidste blok til. 
	int temp = sb-> 


}*/

int length_b(int n)
{
	return (int)floor(log2(n)/2);
}

int length_s(int n)
{
	return length_b(n)*((int)floor(log2(n)));
}

int main(int argc, char **argv){
	struct SuperBlock *sb = create_SuperBlock(4,32);
	printf("sb->size %d\n",sb->size);

	int i;
	for(i = 0; i < 4; i++)
		printf("sb->R_b[%d]=%d\n",i,sb->R_b[i]);

	free_SuperBlock(sb);




	return 0;
}
