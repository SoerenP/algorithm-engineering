#ifndef _bitmap_h
#define _bitmap_h

#define Bit_2_int_ratio 32

struct Bitmap {
	int *B; //the bitvector
	int bits;
	int B_size;
};

struct Bitmap *create_bitmap(int bits);
struct Bitmap *random_bitmap(int bits);
void free_bitmap(struct Bitmap *bm);
int fromBitmapToInt32(struct Bitmap *bm, int start_bit, int end_bit);
int fromBitmapToInt(struct Bitmap *bm, int start_bit, int end_bit);
int fromBitmapToInt32Const(struct Bitmap *bm, int start_bit, int end_bit);

void set_bitmap(struct Bitmap *b, int k);
void set_bitmap_l(struct Bitmap *b, long k);
void clear_bitmap(struct Bitmap *b, int k);
int test_bitmap(struct Bitmap *b, int k);
void print_bitmap(struct Bitmap *b);
int naive_rank_bitmap(struct Bitmap *b, int m);
int naive_rank_bitmap_subrange(struct Bitmap *b, int start_bit, int end_bit);
int naive_select_bitmap(struct Bitmap *b, int m);

#endif
