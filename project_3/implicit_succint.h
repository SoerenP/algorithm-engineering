#ifndef _implicit_succint_h
#define _implicit_succint_h

#include <stdio.h>
#include <stdlib.h>

/* structs */

struct block {
	int start_bit;
	int end_bit;
	int size_bits;
};

struct superblock {
	int start_block;
	int end_block;
	int size_blocks;
};
	
struct R_p {
	struct Bitmap **S; //Array af alle mulige bitstrenge af længde b
	int **ranks; //matrice til [streng s, rank i] 
};

struct impl_succ {
	int block_size;
	int superblock_size;
	int block_count;
	int superblock_count;
	int *R_s;
	int *R_b;
	struct block **blocks;
	struct superblock **superblocks;
	struct Bitmap *bm;
};

/* creator, destructor */
struct block *create_block(int start_bit, int end_bit);
struct superblock *create_superblock(int start_block, int end_block);
struct impl_succ *create_impl_succ(int blocksize, int superblocksize, struct Bitmap *bm);
void free_impl_succ(struct impl_succ *die);
void setup_impl_succ(struct impl_succ *is);
void setup_impl_succ_precomputed(struct impl_succ *is, int *precomputed);

/* set, clear bit */
void block_set_bit(struct Bitmap *bm, int block, int bit, int blocksize);
void superblock_set_bit(struct Bitmap *bm, int sblock, int block, int bit, int sblocksize, int blocksize);
void block_clear_bit(struct Bitmap *bm, int block, int bit, int blocksize);
void superblock_clear_bit(struct Bitmap *bm, int sblock, int block, int bit, int sblocksize, int blocksize);
int block_test_bit(struct Bitmap *bm, int block, int bit, int blocksize);
int superblock_test_bit(struct Bitmap *bm, int sblock, int block, int bit, int sblocksize, int blocksize);

/* conversions */
int fromBitmapToInt(struct Bitmap *bm, int start_bit, int end_bit);
struct Bitmap *fromIntToBitmap(int target, int bits);

/* rank O(1), select O(lg lg n) */
int implicit_succint_select(struct impl_succ *is, int i, int bitmap_size, size_t n, size_t m, int R_p[n][m]);
int implicit_succint_rank(struct impl_succ *is, int i, size_t n, size_t m, int R_p[n][m]);

/* helpers */
int length_b(int n);
int length_s(int n);

#endif
