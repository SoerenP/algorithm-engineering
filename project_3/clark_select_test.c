#include "clark_select.h"
#include "bitmap.h"
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>


int main(int argc, char **argv){

  // seed randomizer, ONLY ONCE!
  srand(time(NULL));
  if (argc == 2)
    {
      int size = atoi(argv[1]);
      struct Bitmap *bm = create_bitmap(size); // skal den ha size + 1? vi bruger jo ikke 0'te bit
	
      printf("log2(n)log2log2(n)'th bit = %d\n", (int)(ceil(log2((double)size)) * ceil(log2(log2((double)size)))));
	
      int i,n_ones, res1, res2, disagree;
      n_ones = 0;
      printf("Setup...\n");
      printf("\tSetting Random Bits...\n");
      // sæt random bits i bitmap
      for(i = 0; i < size; i++)
	{
	  if((rand() % 2) > 0)
	    {
	      set_bitmap(bm,i);
	      n_ones++;
	    } 
	}	
      printf("ones %d\n",n_ones);

      struct Level1Dir *level1 = build_level_1(bm);
	
      disagree = 0;
      for(i = 1; i <= n_ones; i+=100)
	{
	  res1 = clark_select(i, bm, level1);
	  //printf("clark_select(%d) = %d\n",i,res1);
	  res2 = naive_select_bitmap(bm, i);
	  //printf("naive select(%d) = %d\n",i,res2);
	  if(!(res1 == res2))
	    {
	      printf("naive select(%d) = %d, clark select(%d) = %d\n",i,res2,i,res1);
	      disagree++;
	    }
	}
	
      printf("clark vs naive, disagree # = %d\n",disagree);
      printf("\n\n");

      //print_bitmap(bm);
      free_dirs(level1,size);
      free_bitmap(bm);
    }
  else 
    {
      printf("Call the program with the size of the bitstring you would like to test on\n");
    }
  return -1;
}
