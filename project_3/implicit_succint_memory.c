#include "bitmap.h"
#include "compact.h"
#include "implicit_succint.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <string.h>

int main(int argc, char **argv){
	// stuff for bitmap
	if(argc < 1){
		printf("Usage: implicit <bitvector size>\n");
		return 0;
	}

	int size = atoi(argv[1]);
	size = (int)pow(2,size);	

	struct Bitmap *bm = create_bitmap(size+1); // skal den ha size + 1? vi bruger jo ikke 0'te bit
	int blocksize = length_b(size);
	int superblocksize = length_s(size);
	int block_count = ceil((float)size/(float)blocksize);
	int superblock_count = ceil((float)size/(float)superblocksize);
	int permutations = (int)pow(2,blocksize);	
	printf("Data...\n");	
	printf("\tBitmap[1..%d]\n",size);
	printf("\tblocksize: \t\t%d \tbits\n",blocksize);
	printf("\tsuperblocksize: \t%d \tbits\n",superblocksize);
	printf("\tblock_count: \t\t%d \tblocks\n",block_count);
	printf("\tsuperblock_count: \t%d \tsuperblocks\n",superblock_count);

	int R_p[permutations][blocksize+1];	//OBS blocksize +1
	
	/* lav is strukturen */
	struct impl_succ *is = create_impl_succ(blocksize, superblocksize, bm);
	
	/* free den, se i valgrind hvad vi brugte? */
	free_impl_succ(is);
	free_bitmap(bm);
	
	return 0;
}
