#ifndef _succinct_h
#define _succint_h
#include "bitmap.h"

//divide bit array into blocks of length b = floor[log(n)/2]
struct Block {
	struct Bitmap *bm;
	int size;
};

// SuperBlocks are consecutive blocks, superblock has length s = b floor[log n]
struct SuperBlock {
	struct Block **b; //An array of blocks  
	int *R_b; 	//for hver blok k i superblocken j, 0 <= k <= floor[n/b]
	int size;	//Gemmer vi R_b[k] = rank(B,k*b-rank(B,j*s)
};

struct RRR {
	struct SuperBlocks **s_blocks; //Array af superblocke
	int *R_s; //for hver superblock j, gemmer vi R_s[j] = rank(B,j*s)
	int size;
};


/* prototyper */
struct Block *create_Block(int size);
void free_block(struct Block *b);

struct SuperBlock *create_SuperBlock(int size_sb, int size_b);
int calc_R_b(struct Block *b);

/* helper */
int length_b(int n);
int length_s(int n);

#endif
