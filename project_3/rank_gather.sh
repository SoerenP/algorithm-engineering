set -e
#!/bin/bash
for i in 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20
do
	echo "time rank $i'th"
	/home/soren/CompSci/AlgEng/src_project_3/implicitdata 10 29 1000000 rank 10 0 test_results/time/rank_c_time_1m_10_29v$i.csv
	echo "l1 rank $i'th"
	/home/soren/CompSci/AlgEng/src_project_3/implicitdata 10 29 1000000 rank 10 1 test_results/cache/rank_c_l1_1m_10_29v$i.csv
	echo "l2 rank $i'th"
	/home/soren/CompSci/AlgEng/src_project_3/implicitdata 10 29 1000000 rank 10 2 test_results/cache/rank_c_l2_1m_10_29v$i.csv
	echo "tlb rank $i'th"
	/home/soren/CompSci/AlgEng/src_project_3/implicitdata 10 29 1000000 rank 10 3 test_results/tlb/rank_c_tlb_1m_10_29v$i.csv
	echo "branch rank $i'th"
	/home/soren/CompSci/AlgEng/src_project_3/implicitdata 10 29 1000000 rank 10 4 test_results/branch/rank_c_branch_1m_10_29v$i.csv
	echo "instr rank $i'th"
	/home/soren/CompSci/AlgEng/src_project_3/implicitdata 10 29 1000000 rank 10 5 test_results/instr/rank_c_instr_1m_10_29v$i.csv
done



