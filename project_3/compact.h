#ifndef _compact_h
#define _compact_h

void set_bit(int *A, int k);
void set_bit_l(int *A, long k);
void clear_bit(int *A, int k);
int test_bit(int *A, int k);
void print_bits(int *A, int size);
void test_bits(int size, int *to_set, int setsize, int *to_clear, int clearsize);


/* naive implementation => O(n) rank/select query time, n space usage */
int naive_rank(int *A, int size, int m);
int naive_select(int *A, int size, int m);
int naive_rank_subrange(int *A, int size, int start_bit, int end_bit);
#endif
