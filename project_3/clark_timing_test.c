#include "clark_select.h"
#include "bitmap.h"
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>


int main(int argc, char **argv){

  // seed randomizer, ONLY ONCE!
  srand(time(NULL));
  if (argc == 2)
    {
      int size = atoi(argv[1]);

      printf("Size is %d\n", size);
      struct Bitmap *bm = create_bitmap(size); // skal den ha size + 1? vi bruger jo ikke 0'te bit
	
      printf("log2(n)log2log2(n)'th bit = %d\n", (int)(ceil(log2((double)size)) * ceil(log2(log2((double)size)))));
	
      int j,n_ones;
      n_ones = 0;
      printf("Setup...\n");
      printf("\tSetting Random Bits...\n");
      // sæt random bits i bitmap
      for(j = 0; j < size; j++)
	{
	  if((rand() % 2) > 0)
	    {
	      set_bitmap(bm,j);
	      n_ones++;
	    } 
	}	
      printf("ones %d\n",n_ones);

      struct Level1Dir *level1 = build_level_1(bm);
    
      clock_t begin, end;
      double time_spent;

      /* start counting */
      begin = clock();

      /* do work */ 
      for(j = 0; j < 1000000; j++)
	{
	  clark_select(rand()%n_ones+1, bm, level1);
	}
      /*stop counting */
      end = clock();
      time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
      printf("It took %f seconds to do 1000000 queries\n", time_spent);
    
      //print_bitmap(bm);
      free_dirs(level1,size);
      free_bitmap(bm);
    } 
  else 
    { 
      printf("Call the test with a number equal to the size of the bitstring you want to test on\n");
    }
  return -1;
}
