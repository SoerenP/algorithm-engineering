#ifndef _clark_select_h
#define _clark_select_h

#include "bitmap.h"


struct Level1Dir{

  struct Level2Dir* subdir;
  int size;
  int pos;
};

struct Level2Dir{

  struct Level3Dir* subdir;
  int pos;
};

struct Level3Dir{

  int** pos_of_ones;
  int* num_ones;
  int* subpos;
  int pos;
};


int clark_select(int index, struct Bitmap* bitmap, struct Level1Dir* level1);

struct Level1Dir* build_level_1(struct Bitmap* bitmap);

void build_level_2(struct Bitmap* bitmap, struct Level2Dir* level_2, int r, int pos, int lgn, int lglgn);

void build_level_3(struct Bitmap* bitmap, struct Level3Dir* level_3, int r_mark, int r, int pos, int num_ones_in_level_2_subdir, int lgn, int lglgn, int lgr);

int count_ones(unsigned int bitstring);

int* find_pos_of_ones(unsigned int bitstring, int size);

void free_dirs(struct Level1Dir* level1, int n);

#endif
