#include <stdio.h>
#include <time.h>
#include <assert.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <limits.h>
#include <math.h>
#include "clark_select.h"
#include "papi_wrap.h"
#include "papi.h"
#include "file_io.h"

#define BRANCH_RES_PATH "test_results/branch/"
#define L1_RES_PATH "test_results/l1_cache/"
#define L2_RES_PATH "test_results/l2_cache/"
#define TIME_RES_PATH "test_results/time/"
#define INSTR_RES_PATH "test_results/instr/"


/* Setup arrays of events to PAPI */
#define event_number 1
#define query_number 1000000

long long values[event_number]; //Array for results
int events[event_number] = {PAPI_L1_DCM};//, PAPI_L2_DCM, PAPI_TLB_TL, PAPI_BR_MSP};

double time_select(struct Bitmap* bm, struct Level1Dir* level1, int n_ones)
{

  clock_t begin, end;
  double time_spent;

  /* start counting */
  begin = clock();

  /* do work */
  int i, res;
  for(i = 0; i < query_number; i++)
    {
      res = clark_select(rand()%n_ones+1, bm, level1);
    }
  /*stop counting */
  end = clock();

  printf("Returned from time\n");
  time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
  return time_spent;
}

long long L1_misses_select(struct Bitmap* bm, struct Level1Dir* level1, int n_ones){
  
  long long values[2]; //Array for results
  int events[1] = {PAPI_L1_DCM};
  init_PAPI(events,1);
   /* do work */
  int i, res;
  printf("Called L1\n");
  for(i = 0; i < query_number; i++)
    {
      res = clark_select(rand()%n_ones+1, bm, level1);
    }
  printf("Returned from L1\n");
  stop_PAPI(values,1);
  
  return values[0];
}

long long L2_misses_select(struct Bitmap* bm, struct Level1Dir* level1, int n_ones){
  long long values[2]; //Array for results
  int events[1] = {PAPI_L2_DCM};
  init_PAPI(events,1);
   /* do work */
  int i, res;
  for(i = 0; i < query_number; i++)
    {
      res = clark_select(rand()%n_ones+1, bm, level1);
    }
  stop_PAPI(values,1);
  return values[0];
}

long long TLB_misses_select(struct Bitmap* bm, struct Level1Dir* level1, int n_ones){
  long long values[2]; //Array for results
  int events[1] = {PAPI_TLB_TL};
  init_PAPI(events,1);
  /* do work */
  int i;
  for(i = 0; i < query_number; i++)
    {
      clark_select(rand()%n_ones+1, bm, level1);
    }
  stop_PAPI(values,1);
  return values[0];
}

long long branch_mispred_select(struct Bitmap* bm, struct Level1Dir* level1, int n_ones){
long long values[2]; //Array for results
  int events[1] = {PAPI_BR_MSP};
  init_PAPI(events,1);
  /* do work */
  int i,res;
  for(i = 0; i < query_number; i++)
    {
      res = clark_select(rand()%n_ones+1, bm, level1);
    }
  stop_PAPI(values,1);
  return values[0];
}

long long total_instructions_select(struct Bitmap* bm, struct Level1Dir* level1, int n_ones){
  long long values[2]; //Array for results
  int events[1] = {PAPI_TOT_INS};
  init_PAPI(events,1);
   /* do work */
  int i,res;
  for(i = 0; i < query_number; i++)
    {
      res = clark_select(rand()%n_ones+1, bm, level1);
    }
  stop_PAPI(values,1);
  return values[0];
}

//, PAPI_L2_DCM, PAPI_TLB_TL, PAPI_BR_MSP};


void run_test(char* fileversion)
{
  /* setup values for data gathering */
  int i,k;
  int const high = 31;
  int const low = 10;
  int const iter = 1;
  
  int sizes[high-low];
  double times[high-low][iter];
  long long L1[high-low][iter];
  long long L2[high-low][iter];
  //long long TLB[high-low];
  long long BR_MSP[high-low][iter];
  long long TOT_INS[high-low][iter];
	
  for(i = low; i < high; i++){
    sizes[i-low] = i;
      int n = 1 << i;
      printf("\n\n STARTING NEW ITERATION WITH N = %d\n\n", n);
    for (k = 0; k < iter; k++){

      struct Bitmap *bm = create_bitmap(n);

      int j,n_ones;
	n_ones = 0;
	printf("Setup...\n");
	printf("\tSetting Random Bits...\n");
	// sæt random bits i bitmap
	for(j = 0; j < n; j++)
	{
		if((rand() % 2) > 0)
		{
			set_bitmap(bm,j);
			n_ones++;
		} 
	}	
	printf("\tones %d\n",n_ones);

	printf("\tBuilding dirs... \n");
	struct Level1Dir *level1 = build_level_1(bm);
	printf("Running test...\n");
	printf("\tQuerying...\n");

	times[i-low][k] = time_select(bm,level1,n_ones);
      printf("\tRan 1000000 queries, and it took %f seconds\n", times[i-low][k]);
      L1[i-low][k] = L1_misses_select(bm,level1,n_ones);
      printf("\tL1 cache misses: %lld\n", L1[i-low][k]);
      L2[i-low][k] = L2_misses_select(bm,level1,n_ones);
      printf("\tL2 cache misses: %lld\n", L2[i-low][k]);
      //TLB[j] = TLB_misses_mult(A, B, ml, mult);
      //printf("TLB cache misses: %lld\n", TLB[j]);
      BR_MSP[i-low][k] = branch_mispred_select(bm,level1,n_ones);
      printf("\tBranch mispredictions: %lld\n", BR_MSP[i-low][k]);
      TOT_INS[i-low][k] = total_instructions_select(bm,level1,n_ones);
      printf("\tTotal number of instructions: %lld\n", TOT_INS[i-low][k]);

      printf("Cleaning up...\n");
      free_dirs(level1,n);
      free_bitmap(bm);
    }
  }
  /* write results to csv files */
  char timefile[128];
  char filename[] = "clark_select.csv";
  sprintf(timefile, "%s%s%s", TIME_RES_PATH, fileversion, filename);
  print_f_matrix_ToCSV(sizes,high-low,iter,times,timefile);

  char L1file[128];
  sprintf(L1file, "%s%s%s", L1_RES_PATH, fileversion, filename);
  print_ll_matrix_ToCSV(sizes,high-low,iter,L1,L1file);

  char L2file[128];
  sprintf(L2file, "%s%s%s", L2_RES_PATH, fileversion, filename);
  print_ll_matrix_ToCSV(sizes,high-low,iter,L2,L2file);

  char branchfile[128];
  sprintf(branchfile, "%s%s%s", BRANCH_RES_PATH, fileversion, filename);
  print_ll_matrix_ToCSV(sizes,high-low,iter,BR_MSP,branchfile);

  char instrfile[128];
  sprintf(instrfile, "%s%s%s", INSTR_RES_PATH, fileversion, filename);
  print_ll_matrix_ToCSV(sizes,high-low,iter,TOT_INS,instrfile);

}

int main(int argc, char **argv){
  
  
  /* seed randomizer, ONLY ONCE! */
  srand(time(NULL));
  if (argc == 2)
    run_test(argv[1]);
  else
    printf("Run the program with the string you want to prefix the resulting files with\n");
  return 1;
}
