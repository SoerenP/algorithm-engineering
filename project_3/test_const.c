#include "implicit_succint.h"
#include "bitmap.h"
#include "compact.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int main(int argc, char **argv){

	struct Bitmap *bm = create_bitmap(64);
	
	set_bitmap(bm,1);
	set_bitmap(bm,2);
	set_bitmap(bm,33);
	set_bitmap(bm,32);
	set_bitmap(bm,31);
	
	print_bitmap(bm);
	
	int res = fromBitmapToInt32Const(bm,30,33);
	printf("res %d\n",res);

	free_bitmap(bm);
	return 0;
}
