#include <stdio.h>
#include <stdlib.h>
#include "limits.h"
#include "bitmap.h"
#include "compact.h"
#include <math.h>


#define MAX(x, y) (((x) > (y)) ? (x) : (y))
#define MIN(x, y) (((x) < (y)) ? (x) : (y))


/* given amount of bits wanted, creates array of int to encompass these */
/* one int is 4 byte = 32 bits */
/* bit k is at the arrayindex A[k/32] */
/* and in that 4 bytes = 32 bits, its position will be k % 32 */
/* the compact module implements these mappings, so this is just a WRAPPER */
struct Bitmap *create_bitmap(int bits)
{
	struct Bitmap *new = malloc(sizeof(struct Bitmap));
	int i,size;	
	size = ceil(((float)bits/(float)Bit_2_int_ratio));
	if(size < 1) size = 1;

	//fprintf(stderr,"Size %d, bits %d\n",size,bits);
	if(new){
		new->B = malloc(sizeof(int)*size);
		if(new->B){
			//Make DAMN sure that the bitmap is all 0 bits. 
			for(i=0; i < size; i++)
				new->B[i] = 0;
		} else return NULL;
	} else {
		fprintf(stderr,"Failed to create bitmap\n");
		return NULL;
	}
	new->bits = bits;
	new->B_size = size;

	return new;
}

struct Bitmap *random_bitmap(int bits)
{
	struct Bitmap *new = malloc(sizeof(struct Bitmap));
	int i, size;
	size = ceil(((float)bits/(float)Bit_2_int_ratio));
	if(size < 1) size = 1;
	
	if(new)
	{
		new->B = malloc(sizeof(int)*size);
		if(new->B)
		{
			for(i=0; i < size; i++)
			{
				new->B[i] = rand(); //set to a random int, such that the bits will be "random".
			}
		} else return NULL;
	} else {
		fprintf(stderr,"Failed to create random bitmap\n");
		return NULL;
	}
	new->bits = bits;
	new->B_size = size;
		
	return new;	
}



int fromBitmapToInt32(struct Bitmap *bm, int start_bit, int end_bit)
{
	unsigned int i, sum, end, bitsize;
	sum = 0;
	bitsize = 1;
	end = MIN(end_bit-start_bit,Bit_2_int_ratio-1);
	for(i = 0; i <= end; i++)
	{
		sum += test_bitmap(bm,i+start_bit) * bitsize;
		bitsize=bitsize<<1;
	}
	return sum;
}

int fromBitmapToInt32Const(struct Bitmap *bm, int start_bit, int end_bit)
{
  int start_block = start_bit/32;
  int end_block = end_bit/32;
  int start_offset = start_bit%32;
  int end_offset = end_bit%32;

	//printf("s_b %d, e_b %d, s_o %d, e_o %d\n",start_block, end_block, start_offset, end_offset);

  if (start_block == end_block)
    {
    	unsigned one = 1;
    	unsigned mask = (one<<(end_offset+1))- 1 - (one<<(start_offset)) + 1;
      //printf("mask %u\n",mask);
      return (bm->B[start_block] & mask)>>start_offset;
    } 
  else 
    {
      unsigned zero = 0;
      unsigned start_mask = zero-1 - ((1<<(start_offset))-1);
      unsigned end_mask = ((1<<(end_offset+1))-1);
      unsigned most_significant_bits = (bm->B[end_block] & end_mask)<<(32-start_offset);
      unsigned least_significant_bits = (bm->B[start_block] & start_mask)>>start_offset;
      //printf("s_mask %u\n",start_mask);
      //printf("e_mask %u\n",end_mask);
      //printf("msbs %u\n",most_significant_bits);
      //printf("lsbs %u\n",least_significant_bits);
      
      
      return most_significant_bits + least_significant_bits;
    }
  return -1;


}

int fromBitmapToInt(struct Bitmap *bm, int start_bit, int end_bit)
{
	int i, sum;
	sum = 0;
	for(i = 0; i <= end_bit-start_bit; i++)
	{
		if(test_bitmap(bm,start_bit+i))
		{
			sum += (int)(pow(2,i));
		}
	}
	return sum;
}


void free_bitmap(struct Bitmap *bm)
{
	if(bm && bm->B)
	{
		free(bm->B);
		free(bm);
	}
}


void set_bitmap(struct Bitmap *b, int k)
{
	set_bit(b->B,k);
}

void set_bitmap_l(struct Bitmap *b, long k)
{
	set_bit_l(b->B,k);
}

void clear_bitmap(struct Bitmap *b, int k)
{
	clear_bit(b->B,k);
}

int test_bitmap(struct Bitmap *b, int k)
{
	return test_bit(b->B,k);
}

void print_bitmap(struct Bitmap *b)
{
	print_bits(b->B,b->bits);
}

int naive_rank_bitmap(struct Bitmap *b, int m)
{
	return naive_rank(b->B,b->bits, m);
}

int naive_rank_bitmap_subrange(struct Bitmap *b, int start_bit, int end_bit)
{
	return naive_rank_subrange(b->B,b->bits,start_bit,end_bit);
}

int naive_select_bitmap(struct Bitmap *b, int m)
{
	return naive_select(b->B,b->bits,m);
}
