#include "bitmap.h"
#include "compact.h"
#include "implicit_succint.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <string.h>




struct block *create_block(int start_bit, int end_bit)
{
	struct block *new = malloc(sizeof(struct block));
	new->start_bit = start_bit;
	new->end_bit = end_bit;
	new->size_bits = end_bit-start_bit+1;
	return new;
}

struct superblock *create_superblock(int start_block, int end_block)
{
	struct superblock *new = malloc(sizeof(struct superblock));
	new->start_block = start_block;
	new->end_block = end_block;
	new->size_blocks = end_block - start_block + 1;
	return new;
}

struct R_p *create_R_p(int blocksize)
{
	struct R_p *new = (struct R_p *)malloc(sizeof(struct R_p *));
	new->S = (struct Bitmap **) malloc(sizeof(struct Bitmap *));
	int i;
	for(i = 0; i < ((int)pow(2,blocksize)); i++)
	{
		new->S[i] = (struct Bitmap *)create_bitmap(blocksize);
	}
	new->ranks = (int **) malloc(sizeof(int *));
	for(i = 0; i < ((int)pow(2,blocksize)); i++)
	{
		new->ranks[i] = (int *) malloc(sizeof(int)*blocksize);
	}
	return new;

}

struct impl_succ *create_impl_succ(int blocksize, int superblocksize, struct Bitmap *bm)
{
	//int i;
	struct impl_succ *new = malloc(sizeof(struct impl_succ));
	
	/* setup counters */
	new->block_size = blocksize;
	new->superblock_size = superblocksize;
	new->block_count = ceil((float)bm->bits/(float)blocksize);
	new->superblock_count = ceil((float)bm->bits/(float)superblocksize);

	/* setup lookup tables */
	new->R_s = malloc(sizeof(int)*(new->superblock_count+1)); //
	new->R_b = malloc(sizeof(int)*(new->block_count+1)); //

	new->bm = bm;
	return new;
}

void free_impl_succ(struct impl_succ *die)
{
	/* free lookup tables */
	free(die->R_s);
	free(die->R_b);
	free(die);
}

void setup_impl_succ_precomputed(struct impl_succ *is, int *precomputed)
{
	/* for each superblock j, we store a number R_s[j] = rank(B,j*s) */
	printf("\tCreating R_s...\n");
	int j;
	is->R_s[0] = 0; // By definition
	for(j = 1; j <= is->superblock_count; j++)
	{
		is->R_s[j] = precomputed[j*is->superblock_size];//naive_rank_bitmap(is->bm,j*is->superblock_size);
	}

	printf("\tCreating R_b...\n");
	int k,bits;
	bits = is->bm->bits;
	is->R_b[0] = 0; // by definition
	for(k = 1; k <= is->block_count; k++)
	{
		j = k/((int)floor(log2(bits)));
		is->R_b[k] = precomputed[k*is->block_size] - is->R_s[j];
	}

}

void setup_impl_succ(struct impl_succ *is)
{
	/* for each superblock j, we store a number R_s[j] = rank(B,j*s) */
	printf("\tCreating R_s...\n");	
	int j;
	is->R_s[0] = 0; // By definition
	for(j = 1; j <= is->superblock_count; j++)
	{
		is->R_s[j] = naive_rank_bitmap(is->bm,j*is->superblock_size);
	}

	/*
	 * for each block k of superblock j = k div floor(log n), 0 <= k >= floor(n/b) 
	 * we store a number R_b[k] = rank(B,k*b) - rank(B,j*s)
	 */
	printf("\tCreating R_b...\n");
	int k,bits;
	bits = is->bm->bits;
	is->R_b[0] = 0; // by definition
	for(k = 1; k <= is->block_count; k++)
	{
		j = k/((int)floor(log2(bits)));
		is->R_b[k] = naive_rank_bitmap(is->bm,k*is->block_size) - is->R_s[j];
	}

	// Alternativt hurtigere?
	/*int sb, b;		
	is->R_b[0] = 0;
	for(k = 1; k <= is->block_count; k++)
	{
		j = k/((int)floor(log2(bits)));
		sb = (k*is->block_size)/is->superblock_size;
		is->R_b[k] = (is->R_b[k-1] + is->R_s[sb] + naive_rank_bitmap_subrange(is->bm,is->block_size*(k-1)+1, k*is->block_size)) - is->R_s[j];
	}*/

}

int implicit_succint_rank(struct impl_succ *is, int i, size_t n, size_t m, int R_p[n][m])
{
	int b = is->block_size;
	int s = is->superblock_size;
	//return is->R_s[i/s]+is->R_b[i/b]+R_p[fromBitmapToInt(is->bm,((i / b)*b+1),(i/b)*b+b)][(i%b)];
	//return is->R_s[i/s]+is->R_b[i/b]+R_p[fromBitmapToInt32(is->bm,((i / b)*b+1),(i/b)*b+b)][(i%b)];
	return is->R_s[i/s]+is->R_b[i/b]+R_p[fromBitmapToInt32Const(is->bm,((i / b)*b+1),(i/b)*b+b)][(i%b)];
}

int implicit_succint_select(struct impl_succ *is, int i, int bitmap_size, size_t n, size_t m, int R_p[n][m])
{

	/* find superblock (index of predecessor of i, in R_s */ 
	int sb;
	int remaining_size = is->superblock_count;
	int index = 0;
	int done = 0;
	/* binary search after superblock */
	while(!done){
		if(i > is->R_s[index+remaining_size/2]){
			if(i <= is->R_s[index+remaining_size/2+1])// <=?
			{
				sb = index+remaining_size/2;
				done = 1;
			}
			index = index+remaining_size/2;
		} else if(i < is->R_s[index+remaining_size/2]) {
			if(i > is->R_s[index+remaining_size/2-1])//
			{
				sb = index+remaining_size/2-1;
				done = 1;
			}
			remaining_size = remaining_size/2;	
		}
		else
		{
			sb=index+remaining_size/2-1;
			done = 1;
		}
		if(remaining_size <=1){
			//index = index;;
			sb = index;			
			done = 1;
		}
	}
	
	/* find previous block wrt. block in which we reside (in the following superblock) */	
	/* using sequential scan */
	done = 0;
	index = ceil(((float)sb*is->superblock_size+1)/(float)is->block_size);
	remaining_size = ceil(((float)(sb+1)*is->superblock_size)/((float)is->block_size));
	int b,bi;
	int j;

	for(j = index-1; j < remaining_size; j++)
	{
		if(is->R_s[sb]+is->R_b[j] < i)
		{
			b = j;
		} else {
			break;
		}
	}

	/* kig i specifik block (block b+1) */
	//printf("supberblock sp %d, block b: %d\n",sb,b);
	done = 0;
	index = b*is->block_size+1; //starting bit in block in bitmap
	bi = 0;
	remaining_size = index+is->block_size-1; //ending bit in the block
	while(!done)
	{
		if(i == is->R_s[sb] + is->R_b[b] + R_p[fromBitmapToInt(is->bm,index,remaining_size)][bi])
		{
			done = 1;
			return sb*is->superblock_size+(b*is->block_size-sb*is->superblock_size)+bi;
		} else {
			bi++;
		}
		if((index+bi)-1 > remaining_size){
			break;
		}
		if(bi > is->bm->bits){
			break;
		}
	}
	//printf("\n");*/
	return -1;
}


struct Bitmap *fromIntToBitmap(int target, int bits)
{
	int i;
	int A[1] = {target}; 
	struct Bitmap *new = create_bitmap(bits+1);

	for(i = 0; i < bits; i++)
	{
		if(test_bit(A,i)) set_bitmap(new,i+1);
	}
	return new;
}

/* doesnt work */
int impl_succ_simple_select(struct impl_succ *is, int j, size_t n, size_t m, int R_p[n][m])
{
	int done = 0;
	int end = is->bm->bits;
	int start = 1;
	int i = end/2;
	printf("end: %d, i: %d, j: %d\n",end,i,j);
	while(!done){
		if((implicit_succint_rank(is, start + end/2, n,m, R_p) == j) 
			&& (implicit_succint_rank(is, start + end/2-1, n,m, R_p) == j-1))
		{
			return end/2;
		} else if(implicit_succint_rank(is, start + end/2, n,m, R_p)<j) {
			start = end/2+1;
			//end = end/2;
		} else {
			end = end/2;
		}
		if(end <= 1) break;
		}
	return -1;	
}


void block_set_bit(struct Bitmap *bm, int block, int bit, int blocksize)
{
	set_bitmap(bm, blocksize*(block-1)+bit);
}

void superblock_set_bit(struct Bitmap *bm, int sblock, int block, int bit, int sblocksize, int blocksize)
{
	int global_block = sblocksize*(sblock-1)+block;
	block_set_bit(bm,global_block, bit, blocksize);
}

void block_clear_bit(struct Bitmap *bm, int block, int bit, int blocksize)
{
	clear_bitmap(bm,blocksize*(block-1)+bit);
}

void superblock_clear_bit(struct Bitmap *bm, int sblock, int block, int bit, int sblocksize, int blocksize)
{
	int global_block = sblocksize*(sblock-1)+block;
	block_clear_bit(bm,global_block,bit,blocksize);
}

int block_test_bit(struct Bitmap *bm, int block, int bit, int blocksize)
{
	return test_bitmap(bm,blocksize*(block-1)+bit);
}

int superblock_test_bit(struct Bitmap *bm, int sblock, int block, int bit, int sblocksize, int blocksize)
{
	int global_block = sblocksize*(sblock-1)+block;
	return block_test_bit(bm,global_block,bit,blocksize);
}

int length_b(int n)
{
	return (int)floor(log2(n)/2);
}

int length_s(int n)
{
	return length_b(n)*((int)floor(log2(n)));
}

void test_select(){
	int size = 95;
	struct Bitmap *bm = create_bitmap(size+1);
	int blocksize = length_b(size);
	int superblocksize = length_s(size);
	int block_count = ceil((float)size/(float)blocksize);
	int superblock_count = ceil((float)size/(float)superblocksize);
	int permutations = (int)pow(2,blocksize);	
	int R_p[permutations][blocksize+1];	//OBS blocksize +1
	printf("Data...\n");	
	printf("\tBitmap[1..%d]\n",size);
	printf("\tblocksize: \t\t%d \tbits\n",blocksize);
	printf("\tsuperblocksize: \t%d \tbits\n",superblocksize);
	printf("\tblock_count: \t\t%d \tblocks\n",block_count);
	printf("\tsuperblock_count: \t%d \tsuperblocks\n",superblock_count);
	


	int i,s;
	for(s = 0; s < permutations; s++)
	{
		struct Bitmap *temp = fromIntToBitmap(s,blocksize);
		
		for(i = 0; i <= blocksize; i++)
		{
			R_p[s][i] = naive_rank_bitmap(temp,i);
		}
		free_bitmap(temp);
	}

	
	//få den til at ligne den vi har håndkørt.
	set_bitmap(bm,1);
	set_bitmap(bm,4);
	set_bitmap(bm,5);
	set_bitmap(bm,7);
	set_bitmap(bm,9);
	set_bitmap(bm,10);
	set_bitmap(bm,12);
	set_bitmap(bm,13);
	set_bitmap(bm,14);
	set_bitmap(bm,15);
	set_bitmap(bm,20);
	set_bitmap(bm,21);
	set_bitmap(bm,23);
	//set_bitmap(bm,39);
	set_bitmap(bm,48);
	set_bitmap(bm,89);
	print_bitmap(bm);
	int ones;
	ones = naive_rank_bitmap(bm,size);	


	struct impl_succ *is = create_impl_succ(blocksize, superblocksize, bm);
	setup_impl_succ(is);
	printf("R_s: ");
	for(i = 0; i <= superblock_count; i++)
	{
		printf("%d ",is->R_s[i]);
	}
	printf("\nn");

	printf("R_b: ");
	for(i = 0; i <= block_count; i++)
	{
		printf("%d ",is->R_b[i]);
	}
	printf("\n\n");




	int res1,res2,disagree;
	disagree = 0;
	for(i = 1; i <= ones; i++)
	{
		res1 = implicit_succint_select(is,i,size,permutations, blocksize+1,R_p);
		//int impl_succ_simple_select(struct impl_succ *is, int j, size_t n, size_t m, int R_p[n][m])
		//res1 = impl_succ_simple_select(is, i, permutations, blocksize+1, R_p);
		printf("succ_select(B,%d) = %d",i,res1);

		res2 = naive_select_bitmap(bm, i);
		printf("\tnaiv_select(B,%d) = %d\n",i,res2);

		if(!(res1 == res2))
		{ 
			disagree++;
			printf("succ %d, naiv %d\n",res1,res2);
		}

	}	
	printf("disagreements %d\n",disagree);


	free_impl_succ(is);
	free_bitmap(bm);
}
