#include "papi.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "papi_wrap.h"

/*
 *
 * Setup arrays of events to PAPI 
 * long long values[2]; //Array for results
 * int l1[1] = {PAPI_L1_DCM};
 * int l2[1] = {PAPI_L2_DCM};
 * int tlb[1] = {PAPI_TLB_TL};
 * int branch[1] = {PAPI_BR_MSP};
 * int instr[1] = {PAPI_TOT_INS};
 *
 */


void stop_PAPI(long long *values, int eventsize)
{
	int ret;
	if((ret = PAPI_stop_counters(values,eventsize)) != PAPI_OK) test_fail(__FILE__,__LINE__,"PAPI_stop_counters", ret);
}

void init_PAPI(int *events, int eventsize)
{
		if(PAPI_num_counters() < eventsize) {
		fprintf(stderr, "No hardware counters here, or PAPI not supportned\n");
	exit(1);
	}
	int ret;	
	if((ret = PAPI_start_counters(events,eventsize)) != PAPI_OK) test_fail(__FILE__,__LINE__,"PAPI_start_counters", ret);
}


void init_Time_PAPI(float *real_time, float *proc_time, float *mflops, long long *flpins)
{
	int retval;
	if((retval=PAPI_flops(real_time, proc_time, flpins, mflops))<PAPI_OK)
		test_fail(__FILE__,__LINE__,"PAPI_flops", retval);
}

void test_fail(char *file, int line, char *call, int retval)
{
	fprintf(stderr,"%s\tFAILED\nLine # %d\n", file, line);
	if(retval == PAPI_ESYS){
		char buf[128];
		memset(buf, '\0', sizeof(buf));
		sprintf(buf, "System error in %s:", call);
		perror(buf);
	}
	else if ( retval > 0) {
		fprintf(stderr,"Error calculating: %s\n", call);
	} 
	else {
		fprintf(stderr,"Error in %s: %s\n", call, PAPI_strerror(retval));
	}
	fprintf(stderr,"\n");
	exit(1);
}

