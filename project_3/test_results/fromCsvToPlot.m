function [] = fromCsvToPlot(target,low,high,perquery)
temp_target = sprintf('%s%s%d%s',target,'v',low,'.csv');
%temp_target = sprintf('%s%s',target,'.csv')
temp = csvread(temp_target);
rows = size(temp(:,1));
rows = rows(1,1);
columns = size(temp(1,2:end));
columns = columns(1,2);

data = zeros(rows,1);
datasizes = temp(:,1);

%sum all the data
for n=low:1:high
    targetString = sprintf('%s%s%d%s',target,'v',n,'.csv');
    temp = csvread(targetString);
    
    for i=1:1:rows
       data(i,1) = data(i,1) + sum(temp(i,2:end));
    end
end

%average it out by dividing
for i=1:1:rows
   data(i,1) = data(i,1)/(columns*(high-low+1)); 
end
if perquery == 1
    data = data./1000000;
end
plot(datasizes,data,':bx')
hold on


end
