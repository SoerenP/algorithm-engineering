function [] = plotTime(target,low,high)
temp_target = sprintf('%s%s%d%s',target,'v',low,'.csv');
temp = csvread(temp_target);
rows = size(temp(:,1));
rows = rows(1,1);
columns = size(temp(1,2:end));
columns = columns(1,2);

time = zeros(rows,1);
datasizes = temp(:,1);

%sum all the data
for n=low:1:high
    targetString = sprintf('%s%s%d%s',target,'v',n,'.csv');
    temp = csvread(targetString);
    
    for i=1:1:rows
       time(i,1) = time(i,1) + sum(temp(i,2:end));
    end
end

%average it out by dividing
for i=1:1:rows
   time(i,1) = time(i,1)/(columns*(high-low+1)); 
end

plot(datasizes,time)


end
