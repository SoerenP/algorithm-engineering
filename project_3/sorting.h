#ifndef _sorting_h
#define _sorting_h

void die(const char *message);

typedef int (*compare_cb)(int a, int b);
typedef int *(*sorting_alg)(int *numbers, int count, compare_cb cmp); 
int *bubble_sort(int *numbers, int count, compare_cb cmp);

int sorted_order(int a, int b);
int reverse_order(int a, int b);
int strange_order(int a, int b);

int *insertion_sort(int *numbers, int count, compare_cb cmp);

int *quick_sort(int *numbers, int count, compare_cb cmp);
void quicksort_rec(int *array, int firstIndex, int lastIndex);

void test_sorting(int *numbers, int count, compare_cb cmp, sorting_alg sort);


#endif
